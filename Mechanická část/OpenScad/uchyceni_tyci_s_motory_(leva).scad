$fn=20;

roztectyc = 75;
roztecloz = 25;
tloustkapricky = 10;
base = 25; //base +21


translate ([14.5-base-21+5,0,0]){
    translate ([0,0,-tloustkapricky-5]){
        difference() {
            cube ([150,20,tloustkapricky]);
            union(){
                
                translate([base,10,-10])cylinder(r=2.75,h=100);
                translate([base+32,10,-10])cylinder(r=2.75,h=100);
                translate([base+roztectyc,10,0])cylinder(r=2.75,h=100);
                translate([base+32+roztectyc,10,0])cylinder(r=2.75,h=100);
                }
            }
     difference(){
            translate ([ 0,0,40 ]) rotate ([ 0,90,0 ]) cube ([120,20,10]);
      union(){
          translate ([ 0,10,30 ])rotate ([0,90,0])cylinder(r=2.2,h=20,center = true);
          translate ([ 0,10,-70 ])rotate ([0,90,0])cylinder(r=2.2,h=20,center = true);
          }
     }
        

        
          
    }
    translate ([base+roztectyc-10,-30-10,15]) rotate ([0,-90,0])  motorplate();
        translate ([base+roztectyc-10,-30-10,-45]) rotate ([0,-90,0])  motorplate();
    translate ([base+roztectyc-10,8,-35]) rotate ([0,180,0]) cube([10,12,40]);
translate ([40+5,8,-35]) rotate ([0,180,0]) cube([10,12,40]);

translate ([base+roztectyc-10,8,45]) rotate ([0,180,0]) cube([10,12,60]);
translate ([10,8,-65]) rotate ([0,90,0]) cube([10,12,80]);
    
    
//zesileni

translate([0,0,-20]){
    translate([45,-65,10]) mirror([0,1,0]) rotate([0,90,0]) cube([10,10,90],center = true);
    difference(){
        mirror([0,1,0]) cube([10,85,20]);
         translate ([ 0,-80,10 ])rotate ([0,90,0])cylinder(r=2.2,h=50,center = true);
        }
    //cube([80,20,20]);
    translate([0,-30,0]){
        translate([5,-5,5]) triangle1(30);
        translate([5,10,5])triangle2(30);
        translate([85,-5,5]) mirror([1,0,0]) triangle1(30);
        translate([85,10,5]) mirror([1,0,0]) triangle2(30);
        }
}

}
translate([40+14.5-base-21+5,14,-31]) padd();
translate([85+14.5-base-21+5,14,-31]) padd();

module cubeX(x,z){
    difference(){
        union(){ 
            cube([x,x,z],center=true);
            rotate ([0,0,45]) cube([sqrt(2)*x,sqrt(2)*x,z],center=true);
        }
        union(){
            translate([x,0,0]) cube([x,x,z],center=true);
            translate([-x,0,0])cube([x,x,z],center=true);
            translate([0,x,0])cube([x,x,z],center=true);
            }
        }
    
    }
module triangle2(bas){
    mirror([0,1,0]) triangle1(bas);
    }
module triangle1(bas){
    size = bas*2;
         difference(){
    union(){
    rotate([0,0,45]) cube([size,size,10]);
    //translate ([0,-10,0]) cube([10,sqrt(2)/2*size+10,20]);
        
    }
    union(){
       //translate([0,0,10]) rotate ([0,90,0]) cylinder(r=2.75,h=100);
       rotate([0,0,90]) cube([sqrt(2)*size,sqrt(2)*size,10]);
       translate([0,sqrt(2)/2*size,0])cube([sqrt(2)*size,sqrt(2)*size,10]);
    }
        
       
}
    
    }
module motorplate(){
    difference (){
        translate ([ 0,10,3 ])cube ([ 60, 80, 6],center = true);
        union () {
            cylinder (r = 16/2,h=10); // rotor
            
            //srouby motor
             translate ([10.607,10.607,0]) union() {
                cylinder (r = 4.3/2,h=10);
                 translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
                }
             translate ([-10.607,10.607,0]) union() {
                cylinder (r = 4.3/2,h=10);
                 translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
                }
              translate ([-10.607,-10.607,0]) union() {
                cylinder (r = 4.3/2,h=10);
                 translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
                }
              translate ([10.607,-10.607,0]) union() {
                cylinder (r = 4.3/2,h=10);
                 translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
                }
                
             //srouby case
             translate ([16.07,25.7,0]) union() {
                cylinder (r = 3.2/2,h=10);
                 translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
                } 
             translate ([16.07,-25.7,0]) union() {
                cylinder (r = 3.2/2,h=10);
                 translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
                }   
             translate ([-16.07,25.7,0]) union() {
                cylinder (r = 3.2/2,h=10);
                 translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
                }   
             translate ([-16.07,-25.7,0]) union() {
                cylinder (r = 3.2/2,h=10);
                 translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
             }
             
         }
    }
}
module padd( lenn , trans){
    tloustka = 10;
    rotate ([90,0,0]) rotate([0,90,0]) difference (){ 
        union(){
            cube([12,12,tloustka],center=true);
            translate([0,10,0]) cube([12,12,tloustka],center=true);
        }
        union(){
        cylinder(r=2.5+0.2,h=10,center=true);
        rotate ([0,90,0]) cylinder(r=1.45,h=10);
        }
    }
}
