difference(){
union(){mirror([0,0,1])cube([10,20,120]);
translate([10,0,0]) mirror([1,0,0])cube([40,20,10]);
translate([-20,0,-30]) mirror([1,0,0])cube([10,20,30]);
}
translate([0,0,-40]){
    translate ([ 0,10,30 ])rotate ([0,90,0])cylinder(r=2.2,h=15);
    translate ([ 0,10,-70 ])rotate ([0,90,0])cylinder(r=2.2,h=15);
    translate ([ -6,10,30 ])rotate ([0,90,0])cylinder(r=4,h=10);
    translate ([ -6,10,-70 ])rotate ([0,90,0])cylinder(r=4,h=10);
}
}
translate ([0,0,-15])
difference(){
    mirror([0,1,1])cube([10,20,90]);
    union(){
        translate([0,0,-20])    translate ([ 0,-80,10 ])rotate ([0,90,0])cylinder(r=2.2,h=10);
        translate([-6,0,-20])    translate ([ 0,-80,10 ])rotate ([0,90,0])cylinder(r=4,h=10);
    }
}
        