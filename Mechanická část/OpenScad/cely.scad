$fn=20;
/*
difference() {
    union() {
        cylinder(h=20, r=5, center=true);
        translate([-15,0,-35]) cube([20, 20, 10]);
       
        }
    cylinder(h=20, r=5, center=true);
}
*/
roztectyc = 75;
roztecloz = 25;
tloustkadesky = 5;

/*
translate ([ 0.00, 0.00, 7/4*30 ]){
rotate([ 0.00, 45.00, 0.00 ]){
cube ([ 60, 60, 60]);
}
}
*/

translate([0,200,0]) mirror([0,1,0]){
translate ([14.5-base-21+5,0,0]){
    translate ([0,0,-tloustkapricky-5]){
       difference() {
            cube ([150,20,tloustkapricky]);
            union(){
                
                translate([base,10,-10])cylinder(r=2.75,h=100);
                translate([base+32,10,-10])cylinder(r=2.75,h=100);
                translate([base+roztectyc,10,0])cylinder(r=2.75,h=100);
                translate([base+32+roztectyc,10,0])cylinder(r=2.75,h=100);
                }
            }
     difference(){
            translate ([ 0,0,40 ]) rotate ([ 0,90,0 ]) cube ([120,20,10]);
      union(){
          translate ([ 0,10,30 ])rotate ([0,90,0])cylinder(r=2.2,h=20,center = true);
          translate ([ 0,10,-70 ])rotate ([0,90,0])cylinder(r=2.2,h=20,center = true);
          }
     }
        

        
          
    }
    /*
    translate ([base+roztectyc-10,-30-10,15]) rotate ([0,-90,0])  motorplate();
        translate ([base+roztectyc-10,-30-10,-45]) rotate ([0,-90,0])  motorplate();*/
    translate ([base+roztectyc-10,8,-35]) rotate ([0,180,0]) cube([10,12,40]);
translate ([40+5,8,-35]) rotate ([0,180,0]) cube([10,12,40]);

translate ([base+roztectyc-10,-12,0]) rotate ([0,180,0]) cube([10,12,15]);
translate ([base+roztectyc-10-45,-12,0]) rotate ([0,180,0]) cube([10,12,15]);
translate ([10,8,-65]) rotate ([0,90,0]) cube([10,12,80]);
}
translate([40+14.5-base-21+5,14,-31]) padd();
translate([85+14.5-base-21+5,14,-31]) padd();
translate ([85+14.5-base-21+5,-6,15]) rotate ([180,0,0]) padd();
translate ([40+14.5-base-21+5,-6,15]) rotate ([180,0,0]) padd();
}
translate ([0,50,0]){
translate ([ 0.00, 35.00, 0.00 ]){
    translate([roztectyc, 0.00, 0.00 ]){
         linlozR();
         }
     translate([roztectyc, roztecloz, 0.00 ]){
         linloz();
         }
    translate([0, 0.00, 0.00 ]){
         linlozR();
         }
     translate([0, roztecloz, 0.00 ]){
         linloz();
        }
}
difference (){
    union(){
 translate ([ -10, 0, -tloustkadesky ]) cube ([  29+ roztectyc+20,2*35+ roztecloz, tloustkadesky ]);
    translate ([74,20,-15]) cube([13,52.3,10]);
    translate ([92,20,-15]) cube([13,52.3,10]);
        }
    union(){
        loziskodiry(); 
        translate ([roztectyc,0,0]) loziskodiry(); 
        translate ([roztectyc,roztecloz+10+25,0]) loziskodiry(); 
        translate ([0,roztecloz+10+25,0]) loziskodiry();
        
         translate ([40,40,-15])cylinder(r=2.25,h=30); 
        translate ([40,55,-15])cylinder(r=2.25,h=30); 
        translate ([60,40,-15])cylinder(r=2.25,h=30); 
        translate ([60,55,-15])cylinder(r=2.25,h=30);
         translate ([80.5,67,-25])cylinder(r=1.5,h=50);  
        translate ([98.5,67,-25])cylinder(r=1.5,h=50);  
        translate ([98.5,25,-25])cylinder(r=1.5,h=50);
        translate ([80.5,25,-25])cylinder(r=1.5,h=50);
        }
}
/*
translate ([ 13, 20.00, 0.00 ]){ 
    translate ([ 10.00, 0.00, 0.00 ]) rotacloz1 ();
     rotacloz2();
    }
*/
union(){
translate([8.5,40,-21]) padd();
translate([8.5+45,40,-21]) padd();
translate([8.5,40,-31]) padd();
translate([8.5+45,40,-31]) padd();
}
translate ([ 17+70, 30.00, -10 ]){ 
    translate ([ 5.00, 0.00, 0.00 ]) rotacloz1 ();
     rotacloz2();
    }
//linloz();
//pad(30,0);
}

tloustkapricky = 10;
base = 25; //base +21
translate ([14.5-base-21+5,0,0]){
    translate ([0,0,-tloustkapricky-5]){
        difference() {
            cube ([150,20,tloustkapricky]);
            union(){
                
                translate([base,10,-10])cylinder(r=2.75,h=100);
                translate([base+32,10,-10])cylinder(r=2.75,h=100);
                translate([base+roztectyc,10,0])cylinder(r=2.75,h=100);
                translate([base+32+roztectyc,10,0])cylinder(r=2.75,h=100);
                }
            }
     difference(){
            translate ([ 0,0,40 ]) rotate ([ 0,90,0 ]) cube ([120,20,10]);
      union(){
          translate ([ 0,10,30 ])rotate ([0,90,0])cylinder(r=2.2,h=20,center = true);
          translate ([ 0,10,-70 ])rotate ([0,90,0])cylinder(r=2.2,h=20,center = true);
          }
     }
        

        
          
    }
    translate ([base+roztectyc-10,-30-10,15]) rotate ([0,-90,0])  motorplate();
        translate ([base+roztectyc-10,-30-10,-45]) rotate ([0,-90,0])  motorplate();
    translate ([base+roztectyc-10,8,-35]) rotate ([0,180,0]) cube([10,12,40]);
translate ([40+5,8,-35]) rotate ([0,180,0]) cube([10,12,40]);

translate ([base+roztectyc-10,8,45]) rotate ([0,180,0]) cube([10,12,60]);
translate ([10,8,-65]) rotate ([0,90,0]) cube([10,12,80]);
}
translate([40+14.5-base-21+5,14,-31]) padd();
translate([85+14.5-base-21+5,14,-31]) padd();


module loziskodiry(){
    translate ([-5,5,-15])cylinder(r=1.5,h=30); 
    translate ([-5,5+25,-15])cylinder(r=1.5,h=30);
    translate ([-5+10+29,5,-15])cylinder(r=1.5,h=30);
    translate ([-5+10+29,5+25,-15])cylinder(r=1.5,h=30);
}
module rotacloz1(){
 translate ([0,0, -tloustkadesky]) {
     rotate ([ 180.00, 0.00, 90.00 ]) rotacloz();
     }
 }
 module rotacloz2(){
    rotate([ 0.00, 0.00, -90.00 ]) mirror([ 1.00, 1.00, 0.00 ]) rotacloz1();
 }
module linloz(){
    prumertyc = 10+3;
    prumer = 19+0.3;
    tloustka = 29;
    zahloubeni = 3;
    prekryv = 5;
    
    translate ([+(prumer+2*prekryv)/2,0,(prumer+2*prekryv)/2]){
    rotate ([ -90.00, 0.00, 0.00 ]){
    difference(){     
      translate ([-(prumer+2*prekryv)/2,-(prumer+2*prekryv)/2,0]) {
    union(){  
        difference(){
                difference (){
                    cube([prumer+2*prekryv,prumer+2*prekryv,tloustka+2*zahloubeni]);   
                    translate([+1/4*(prumer+2*prekryv),0,0]){rotate([ 0.00,  0,45.00])
                        { mirror ([1,0,0])cube ([prumer+2*prekryv,prumer+2*prekryv,tloustka+2*zahloubeni]);}}
                }
              translate ([+3/4*(prumer+2*prekryv),0,0])
             {rotate([ 0.00,  0,45.00])      
                 { mirror ([0,1,0])cube ([prumer+2*prekryv,prumer+2*prekryv,tloustka+2*zahloubeni]);}}
          }
          union(){
            pad(tloustka+2*zahloubeni,prumer+2*prekryv);
            translate([ 10+prumer+2*prekryv, 0, 0 ]) pad(tloustka+2*zahloubeni,prumer+2*prekryv);
          }
      }
      
  }       
   translate([0,0,zahloubeni])  union(){
        cylinder(h=tloustka, r=prumer/2);
        translate([0,0,tloustka]) cylinder(h=zahloubeni, r=prumer/2+prekryv);
        cylinder(h=999, r=prumertyc/2,center = true);
        };
   
        
    }
    
}
}
}
module linlozR(){
mirror(v=[ 0, 1, 0 ]){linloz();}
}
module rotacloz(){
    prumertyc = 8+1;
    prumer = 22+0.3;
    tloustka = 7;
    zahloubeni = 3;
    prekryv = 5;
    translate ([+(prumer+2*prekryv)/2,0,(prumer+2*prekryv)/2]){
    rotate ([ -90.00, 0.00, 0.00 ]){
    difference(){     
      translate ([-(prumer+2*prekryv)/2,-(prumer+2*prekryv)/2,0]) {
    union(){  
        difference(){
                difference (){
                    cube([prumer+2*prekryv,prumer+2*prekryv,tloustka+2*zahloubeni]);   
                    translate([+1/4*(prumer+2*prekryv),0,0]){rotate([ 0.00,  0,45.00])
                        { mirror ([1,0,0])cube ([prumer+2*prekryv,prumer+2*prekryv,tloustka+2*zahloubeni]);}}
                }
              translate ([+3/4*(prumer+2*prekryv),0,0])
             {rotate([ 0.00,  0,45.00])      
                 { mirror ([0,1,0])cube ([prumer+2*prekryv,prumer+2*prekryv,tloustka+2*zahloubeni]);}}
          }
          union(){
            pad2(tloustka+2*zahloubeni,prumer+2*prekryv);
            translate([ 10+prumer+2*prekryv, 0, 0 ]) pad2(tloustka+2*zahloubeni,prumer+2*prekryv);
          }
      }
      
  }       
   translate([0,0,zahloubeni])  union(){
        cylinder(h=tloustka, r=prumer/2);
        translate([0,0,tloustka]) cylinder(h=zahloubeni, r=prumer/2+prekryv);
        cylinder(h=999, r=prumertyc/2,center = true);
        };
   
        
    }
    
}
}
}
module pad( lenn , trans){
    tloustka = 4;
    translate ([ -10, trans-tloustka, 0 ]){
        difference (){
            cube([ 10, tloustka, lenn]);
            union(){
                translate([5, 0, 5 ]) rotate ([ -90, 0, 0]) cylinder (h=20,r=3/2);
                translate([5, 0, lenn-5 ]) rotate ([ -90, 0, 0]) cylinder (h=20,r=3/2);
            }
            
        }
        
    
}
 }
 module pad2( lenn , trans){
    tloustka = 4;
    translate ([ -10, trans-tloustka, 0 ]){
        difference (){
            cube([ 10, tloustka, lenn]);
            union(){
                
                translate([5, 0, lenn/2 ]) rotate ([ -90, 0, 0]) cylinder (h=20,r=3/2);
            }
            
        }
        
    
}
 }




module motorplate(){
    difference (){
        translate ([ 0,10,3 ])cube ([ 60, 80, 6],center = true);
        union () {
            cylinder (r = 16/2,h=10); // rotor
            
            //srouby motor
             translate ([10.607,10.607,0]) union() {
                cylinder (r = 4.3/2,h=10);
                 translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
                }
             translate ([-10.607,10.607,0]) union() {
                cylinder (r = 4.3/2,h=10);
                 translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
                }
              translate ([-10.607,-10.607,0]) union() {
                cylinder (r = 4.3/2,h=10);
                 translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
                }
              translate ([10.607,-10.607,0]) union() {
                cylinder (r = 4.3/2,h=10);
                 translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
                }
                
             //srouby case
             translate ([16.07,25.7,0]) union() {
                cylinder (r = 3.2/2,h=10);
                 translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
                } 
             translate ([16.07,-25.7,0]) union() {
                cylinder (r = 3.2/2,h=10);
                 translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
                }   
             translate ([-16.07,25.7,0]) union() {
                cylinder (r = 3.2/2,h=10);
                 translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
                }   
             translate ([-16.07,-25.7,0]) union() {
                cylinder (r = 3.2/2,h=10);
                 translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
             }
             
         }
    }
}
module padd( lenn , trans){
    tloustka = 10;
    rotate ([90,0,0]) rotate([0,90,0]) difference (){ 
        union(){
            cube([12,12,tloustka],center=true);
            translate([0,10,0]) cube([12,12,tloustka],center=true);
        }
        union(){
        cylinder(r=2.5+0.1,h=10,center=true);
        rotate ([0,90,0]) cylinder(r=1.45,h=10);
        }
    }
}
