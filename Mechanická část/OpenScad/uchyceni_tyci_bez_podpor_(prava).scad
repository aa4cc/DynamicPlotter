$fn=10;

roztectyc = 75;
roztecloz = 25;
tloustkapricky = 20;
base = 25; //base +21
translate([]) mirror([0,1,0]){
translate ([14.5-base-21+5,0,0]){
translate ([0,0,-tloustkapricky-5]){
   difference() {
        cube ([150,20,tloustkapricky]);
        union(){
            
            translate([base,10,-10])cylinder(r=2.75,h=100);
            translate([base+32,10,-10])cylinder(r=2.75,h=100);
            translate([base+roztectyc,10,0])cylinder(r=2.75,h=100);
            translate([base+32+roztectyc,10,0])cylinder(r=2.75,h=100);
            }
        }
 difference(){
        translate ([ 0,0,40 ]) rotate ([ 0,90,0 ]) cube ([120,20,10]);
  union(){
      translate ([ 0,10,30 ])rotate ([0,90,0])cylinder(r=2.2,h=20,center = true);
      translate ([ 0,10,-70 ])rotate ([0,90,0])cylinder(r=2.2,h=20,center = true);
      }
 }
    

    
      
}
/*
translate ([base+roztectyc-10,-30-10,15]) rotate ([0,-90,0])  motorplate();
    translate ([base+roztectyc-10,-30-10,-45]) rotate ([0,-90,0])  motorplate();*/
translate ([base+roztectyc-10,8,-35]) rotate ([0,180,0]) cube([10,12,40]);
translate ([40+5,8,-35]) rotate ([0,180,0]) cube([10,12,40]);

translate ([base+roztectyc-10,-12,0]) rotate ([0,180,0]) cube([10,12,15]);
translate ([base+roztectyc-10-45,-12,0]) rotate ([0,180,0]) cube([10,12,15]);
translate ([10,8,-65]) rotate ([0,90,0]) cube([10,12,80]);

}
translate([40+14.5-base-21+5,14,-31]) padd();
translate([85+14.5-base-21+5,14,-31]) padd();
translate ([85+14.5-base-21+5,-6,15]) rotate ([180,0,0]) padd();
translate ([40+14.5-base-21+5,-6,15]) rotate ([180,0,0]) padd();

}


module motorplate(){
difference (){
    translate ([ 0,10,3 ])cube ([ 60, 80, 6],center = true);
    union () {
        cylinder (r = 16/2,h=10); // rotor
        
        //srouby motor
         translate ([10.607,10.607,0]) union() {
            cylinder (r = 4.3/2,h=10);
             translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
            }
         translate ([-10.607,10.607,0]) union() {
            cylinder (r = 4.3/2,h=10);
             translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
            }
          translate ([-10.607,-10.607,0]) union() {
            cylinder (r = 4.3/2,h=10);
             translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
            }
          translate ([10.607,-10.607,0]) union() {
            cylinder (r = 4.3/2,h=10);
             translate ([0,0,4])  cylinder (r = 8.25/2,h=10);
            }
            
         //srouby case
         translate ([16.07,25.7,0]) union() {
            cylinder (r = 3.2/2,h=10);
             translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
            } 
         translate ([16.07,-25.7,0]) union() {
            cylinder (r = 3.2/2,h=10);
             translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
            }   
         translate ([-16.07,25.7,0]) union() {
            cylinder (r = 3.2/2,h=10);
            translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
            }   
         translate ([-16.07,-25.7,0]) union() {
            cylinder (r = 3.2/2,h=10);
            translate ([0,0,4])  cylinder (r = 6.5/2,h=10);
         }
         
     }
}
}
module padd( lenn , trans){
tloustka = 10;
rotate ([90,0,0]) rotate([0,90,0]) difference (){ 
    union(){
        cube([12,12,tloustka],center=true);
        translate([0,10,0]) cube([12,12,tloustka],center=true);
    }
    union(){
    cylinder(r=2.5+0.1,h=10,center=true);
    rotate ([0,90,0]) cylinder(r=1.45,h=10);
    }
}
}
