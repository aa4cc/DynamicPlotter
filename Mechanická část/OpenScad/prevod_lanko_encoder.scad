$fn = 20;
difference(){
    union(){
        cylinder(r =6,h = 6);
        translate([-4,0,0]) cube([8,100,2]);
        translate([0.4,98,0]) cube([3,3,30]);
        translate([-3.4,98,0]) cube([3,3,30]);
    }
    union() {
        cylinder(r =3.1,h = 6);
        translate ([-10,0,3]) rotate([0,90,0]) cylinder(r = 1.4,h=20);
    }
}
