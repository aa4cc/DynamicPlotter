Dynamický plotter, zařízení podobné vozíku se závěsem u něhož je možné měnit délku závěsu a místo zavěšení. 
Cílem projektu je časově optimální kreslení předem zadaných obrazců pomocí zavěšeného kyvadla. 

Software - skripty potřebné ke spolupráci beaglebone s prostředím Simulink, výpočty potřebné k plánování trajektorie a regulátorů
Další popis uvnitř.

Mechanická část - slt soubory použitých částí

ODrive-changed - změněný repozitář Odrivu (https://odriverobotics.com/) pro zrychlení komunikace

Odrive firmware
zkompilované binárky pro Odrive s úpravami protokolu
Jsou zkompilované dvě rychlosti UART Odrive 115200 (základní) a  460800 (vyšší) baud. Samotný Odrive byl upraven, aby měl menší seriový odpor v UART. 

BeagleBone Black
Tato deska byla zvolena především pro zabudovaný čítač kvadratického enkodéru a plnohodným harwarem pro UART. 
Balíček "Embedded Coder Support Package for BeagleBone Black Hardware" byl použit pro 
Vzledem ke kompatibilitě vestavěného čítače kvadratického enkodéru a kompatiblitě s balíčkem Matlabu "Embedded Coder Support Package for BeagleBone Black Hardware" byl obraz Debianu "Debian 7.8 Image 2015-03-01".


Po přeinstalování BeagleBone je nutné nastavit eQEP a UART. 
Nastavení UART bylo bezproblémové.   
Pro nastavení eQEP - (enhanced Quadrature-Encoded Pulse) decoder bylo použito:
https://github.com/teknoman117/beaglebot
https://www.youtube.com/watch?v=4qvnOxG8vko&ab_channel=DrewFustini


Na Windows 10 je pro SSH připojení k BeagleBone nutné nainstalovat ovladače. Pokud je tato instalace neúspěsná, důvodem můžou být nepodepsané drivery pro BeagleBone:
https://stackoverflow.com/questions/52065512/cannot-install-the-driver-of-beaglebone-on-the-windows-10
https://www.howtogeek.com/167723/how-to-disable-driver-signature-verification-on-64-bit-windows-8.1-so-that-you-can-install-unsigned-drivers/


Na BeagleBone Black je možné se připojit přes SSH po usb kabelu na adrese 192.168.7.2, port 22. 
Původní uživatelské jméno je "debian"
Původní uživatelské heslo je "temppwd"

Pokud je BeagleBone vypínán odpojením napájením může dojít k poškození systému. Pro vypnutí je vhodné používat například příkaz "sudo shutdown -h now".