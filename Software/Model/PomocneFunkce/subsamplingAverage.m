function Avg = subsamplingAverage(x,n)
s1 = size(x, 1);      % Find the next smaller multiple of n
m  = s1 - mod(s1, n);
y  = reshape(x(1:m), n, []);     % Reshape x to a [n, m/n] matrix
Avg = transpose(sum(y, 1) / n);  % Calculate the mean over the 1st di
end
