function vxO = StatesToVelX(X)
LO = X(1, :);
DLO = X(2, :);
phiO = X(3, :);
DphiO = X(4, :);
pO = X(5, :);
DpO = X(6, :);
vxO = DpO + sin(phiO).*(DLO - DpO) + cos(phiO).*(LO - pO).*DphiO;
