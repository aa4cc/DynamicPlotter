function statesToCurrents = statesToCurrents(in1,in2,in3)
%STATESTOCURRENTS
%    STATESTOCURRENTS = STATESTOCURRENTS(IN1,IN2,IN3)

%    This function was generated by the Symbolic Math Toolbox version 8.4.
%    19-May-2020 18:52:26

DDLs = in2(2,:);
DDps = in2(1,:);
DLs = in1(2,:);
Dphis = in1(4,:);
Dps = in1(6,:);
Ls = in1(1,:);
b_k = in3(4,:);
b_m = in3(5,:);
b_ms = in3(10,:);
b_v = in3(6,:);
b_vs = in3(9,:);
m_k = in3(1,:);
m_m = in3(3,:);
m_v = in3(2,:);
phis = in1(3,:);
ps = in1(5,:);
t_mL = in3(7,:);
t_mP = in3(8,:);
t2 = cos(phis);
t3 = sin(phis);
t4 = Dphis.^2;
t5 = DDLs.*m_k.*1.0e+2;
t6 = m_k.*t2.*9.81e+2;
t7 = Ls.*m_k.*t4.*1.0e+2;
t8 = m_k.*ps.*t4.*1.0e+2;
statesToCurrents = [(-t5+t6+t7-t8+Dps.*b_v.*1.0e+2-m_k.*sin(phis.*2.0).*(9.81e+2./2.0)+DDps.*m_k.*2.0e+2+DDps.*m_v.*1.0e+2+t3.*t5+t3.*t8+b_vs.*sign(Dps).*1.0e+2-DDps.*m_k.*t3.*2.0e+2-DDps.*m_k.*t2.^2.*1.0e+2-Ls.*m_k.*t3.*t4.*1.0e+2-Dphis.*Ls.*b_k.*t2.*1.0e+2+Dphis.*b_k.*ps.*t2.*1.0e+2)./(t_mP.*1.0e+2);(t5-t6-t7+t8+DLs.*b_m.*1.0e+2+DDLs.*m_m.*1.0e+2-DDps.*m_k.*1.0e+2+b_ms.*sign(DLs).*1.0e+2+DDps.*m_k.*t3.*1.0e+2)./(t_mL.*1.0e+2)];
