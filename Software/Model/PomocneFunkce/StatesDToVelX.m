function vxO = StatesDToVelX(X,DX)
LO = X(1, :);
DLO = DX(1, :);
phiO = X(3, :);
DphiO = DX(3, :);
pO = X(5, :);
DpO = DX(5, :);
vxO = DpO + sin(phiO).*(DLO - DpO) + cos(phiO).*(LO - pO).*DphiO;
