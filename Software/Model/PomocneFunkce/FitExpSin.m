function [Omega,Beta,Amp,Offset,Omega0] = FitExpSin(time,data,DoPlot)
    T0 = min(time);
    time = time-T0;
    if DoPlot>0
        hold off
        figure(round(DoPlot))
        plot(time,data)
    end
    Fs = 1/(time(2)-time(1));            % Sampling frequency                    
    T = 1/Fs;        % Sampling period       
    L = (max(time)-min(time))*Fs;             % Length of signal
    t = (0:L-1)*T;        % Time vector
    Y = fft(data);
    P2 = abs(Y/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    f = Fs*(0:(L/2))/L;
    [value,index]=max(P1(10:end)); 
    freq=f(index+10)*2*pi

%     fo = fitoptions('Method','NonlinearLeastSquares',...
%                      'Lower',[0,0,-Inf,0,-2*pi],...
%                     'Upper',[Inf,Inf,Inf,Inf, +2*pi],...
%                     'StartPoint',[0.02 ,abs(mean(data)-min(data)) mean(data) freq 0],...
%                     'Robust', 'Bisquare',...
%                     'MaxIter',100000, ...
%                     'MaxFunEvals',10000 ...
%                     );
    fo = fitoptions('Method','NonlinearLeastSquares',...
                    'Lower',[0,0,-Inf,0,-2*pi],...
                    'Upper',[Inf,Inf,Inf,Inf, +2*pi],...
                    'StartPoint',[0.02 ,abs(mean(data)-min(data)) mean(data) freq 0],...
                    'Robust', 'Bisquare',...
                    'MaxIter',100000, ...
                    'MaxFunEvals',10000 ...
                    );

    ft = fittype('offset+a*exp(-x*B)*sin(omega*x+omega0)','options',fo);

    [curve2,gof2] = fit(time,data,ft);
    if DoPlot>0
        hold on;
        plot(curve2,'m');
        hold off;
    end
 
    
    
    coefs = (coeffvalues(curve2));
    Beta = coefs(1); Amp = coefs(2); Omega =  coefs(4);Offset = coefs(3);Omega0= coefs(5);
    
