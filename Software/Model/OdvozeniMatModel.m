close all; clear all; clc;
%% Symbolic variables and functions
% m_k - hmotnost kyvadla 
% m_v - hmotnost voz�ku, m_m - setrva�nost motoru, 
% b_m - tlumen� motoru, b_v - tlumen� vz�tka,  b_k - tlumen� kyvadla 
% t_mL - p�evodn� konstanta pro motor 1, t_mP - p�evodn� konstanta pro motor 2 
% g - gravita�n� konstanta
syms m_k m_v m_m b_m b_v b_k t_mL t_mP b_vs b_ms g


% L - delka z�v�su
% varphi - �hel kyvadla
% p - poloha voz�tka
% in - vstupn� proudy jednotliv�ch motor�
syms L(t) varphi(t) p(t) i_L(t) i_p(t)


%% Pozice z�va��

x(t) = (L(t)-p(t))*sin(varphi(t))+p(t)
y(t) = -(L(t)-p(t))*cos(varphi(t))

%% Vyj�d�en� rychlost�

vx(t) = diff(x(t),t);
vy(t) = diff(y(t),t);
vp(t) = diff(p(t),t);
vL(t) = diff(L(t),t);


%% Lagrangeovy rovnice
%       zavazi                        vozitko + jeho motor      motor lanka        
T = 1/2 *m_k* ((vx(t))^2 + vy(t)^2) + 1/2 * (m_v) * vp(t)^2 +  1/2 * m_m * vL(t)^2
V =  g*m_k*y(t)
LaqrFun = T-V

LaqrA = -functionalDerivative(LaqrFun,[varphi L(t) p(t)])
LaqrPhi = -functionalDerivative(LaqrFun,varphi)
LaqrL = -functionalDerivative(LaqrFun,L)
LaqrP = -functionalDerivative(LaqrFun,p)
disA = [-b_k*diff(varphi,t)*(L(t)-p(t))^2 ;...
        -b_m*diff(L,t)+t_mL*i_L  - b_ms*sign(diff(L,t));... 
        -(b_v)*diff(p,t)+t_mP*i_p- b_vs*sign(diff(p,t))... 
        ]
disPhi = -b_k*diff(varphi,t)*(L(t)-p(t))^2 
disL = -b_m*diff(L,t)+t_mL*i_L - b_ms*sigmoidd(diff(L,t)) %
disP = -(b_v)*diff(p,t)+t_mP*i_p  - b_vs*sigmoidd(diff(p,t))%

LeqrDissA = LaqrA - disA
LeqrDissAEq = LaqrA == disA
LeqrDissPhi = LaqrPhi - disPhi
LeqrDissPhiEq = LaqrPhi == disPhi

TahLanko =     m_k*(L(t)-p(t))*diff(varphi,t)^2 ... % odstrediva
                -m_k*diff(L-p,t,2)... %setrvacnost kyvadlo
                - sin (varphi)*m_k*diff(p,t,2)... % setrvacnost neinerialni soustava
                + cos(varphi) * g * m_k
MomentVozik = m_k*(L(t)-p(t))*diff(varphi,t,2)...
                  +2*m_k*diff(varphi,t)*(diff(L,t)-diff(p,t))...
                  + cos (varphi)*m_k*diff(p,t,2)...
                  + sin(varphi) * g * m_k 


 %% Vyjadreni druhych derivaci za ucelem ziskani stavoveho modelu s proudy jako vstupy
syms Ls DLs DDLs phis Dphis DDphis ps Dps DDps inLs inPs
eqs = subs(LeqrDissA, {L(t), diff(L(t), t), diff(L(t), t, t), varphi(t), diff(varphi(t), t), diff(varphi(t), t, t), p(t), diff(p(t), t), diff(p(t), t, t), i_L(t), i_p(t)}, {Ls, DLs, DDLs, phis, Dphis, DDphis, ps, Dps, DDps, inLs, inPs});
eqs = subs(eqs,g,9.81)
[ddl, ddphi, ddp] = solve(eqs, [DDLs, DDphis, DDps]);
[inPse, inLse,nic] = solve(eqs, [inPs, inLs,DDphis]);

ropeForce = subs(TahLanko, {g,L(t), diff(L(t), t), diff(L(t), t, t), varphi(t), diff(varphi(t), t), diff(varphi(t), t, t), p(t), diff(p(t), t), diff(p(t), t, t), i_L(t), i_p(t)}, {9.81,Ls, DLs, DDLs, phis, Dphis, DDphis, ps, Dps, DDps, inLs, inPs});
ropeForce = ropeForce(0)
% state
x = [Ls; DLs; phis; Dphis; ps; Dps];
% control input
u = [inPs; inLs];
accel = [DDps;DDLs]
% physical params
prms = [m_k; m_v; m_m ; b_k; b_m; b_v; t_mL; t_mP; b_vs ; b_ms];
% Dx = f(x, u)
statesToCurrents = [inPse; inLse];
statesToCurrents = simplify(statesToCurrents);        
dynPlotter_statespace_f = [ x(2);  
                            ddl;
                            x(4);
                            ddphi;
                            x(6);
                            ddp;]

dynPlotter_statespace_f = simplify(dynPlotter_statespace_f);                        
                     
matlabFunction(statesToCurrents, 'Vars',{x, accel, prms}, 'File', 'statesToCurrents_gen');
matlabFunction(ropeForce, 'Vars',{x, accel, prms}, 'File', 'ropeForce_gen');
matlabFunction(dynPlotter_statespace_f, 'Vars',{x, u, prms}, 'File', 'dynPlotter_ode_gen');

%% Vyjadreni druhych derivaci za ucelem ziskani stavoveho modelu se zrychlen�m jako vstupy
eqs = subs(LeqrDissPhi, ...
    {L(t), diff(L(t), t), diff(L(t), t, t), varphi(t), diff(varphi(t), t), diff(varphi(t), t, t), p(t), diff(p(t), t), diff(p(t), t, t), i_L(t), i_p(t)},...
    {Ls, DLs, DDLs, phis, Dphis, DDphis, ps, Dps, DDps, inLs, inPs});
eqs = subs(eqs,{m_v, m_m, b_m, b_v, t_mL, t_mP, b_vs, b_ms},{0,0,0,0,0,0,0,0});
m_zavazi= 0.101;
eqs = subs(eqs,{g,m_k,b_k},{9.81, m_zavazi   0.025*m_zavazi})
eqs = subs(eqs,{DDps,DDLs},{inPs   inLs})

%       [m_k;          b_k];)
ddphi = solve(eqs,  DDphis);

% state
x = [Ls; DLs; phis; Dphis; ps; Dps];
% control input
u = [inPs;  inLs];
% physical params
prms = [m_k b_k];
% Dx = f(x, u)
dynPlotter_simple_statespace_f = [ x(2);  
                            inLs;
                            x(4);
                            ddphi;
                            x(6);
                            inPs;]

dynPlotter_simple_statespace_f = simplify(dynPlotter_simple_statespace_f);                        
                        
%matlabFunction(dynPlotter_simple_statespace_f, 'Vars',{x, u, prms}, 'File', 'simple_dynPlotter_ode');
matlabFunction(dynPlotter_simple_statespace_f, 'Vars',{x, u}, 'File', 'simple_dynPlotter_ode_gen');



%% Pro p�epis funkc� do latexu
%{
LeqrDissL = LaqrL - disL
LeqrDissLEq = LaqrL == disL
LeqrDissP = LaqrP - disP
LeqrDissPEq = LaqrP == disP
              
SubstituceLegrP =   cos(varphi(t))*   MomentVozik... %moment
            +(1-sin(varphi))*  TahLanko  % tah na lano
        
 syms F_lano F_moment l(t) y_k(t) x_k(t)       
 LeqrPhiLatex  = latex(simplify(simplify(subs(LeqrDissPhi,{(L),diff((L),t)},{l(t)+p,diff(l+p,t)})),'Criterion','preferReal')==0)
 latex((simplify(simplify(LeqrDissP) - SubstituceLegrP +cos(varphi(t))*   F_moment +(1-sin(varphi))*  F_lano== 0  )))
 latex((simplify(simplify(LeqrDissL) + TahLanko - F_lano  == 0)))
 latex(F_lano == TahLanko) 
 latex(F_moment == MomentVozik)
 Vlatex  = latex(-g*m_k*y_k)
 Tlatex = latex(1/2 *m_k* (diff((x_k(t)),t)^2 + diff((y_k(t)),t)^2) + 1/2 * (m_v) * vp(t)^2 +  1/2 * m_m * vL(t)^2)
 oscilaceLatex  = latex(simplify(simplify(subs(LeqrDissPhi,{diff(L,t),(L),p},{0,l(t),0})),'Criterion','preferReal')==0)
 %}
