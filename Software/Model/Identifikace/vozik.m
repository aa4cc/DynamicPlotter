load('vozik_posun.mat')
%konecna rychlost asi 0.1372 0.1388
dataset=vozik2;
T0 = 1;    %zacatecni �as
T1 = max(dataset.time);    %konecny �as
%T0 = 46.95;    %zacatecni �as
%T1 = 48.67;    %konecny �as
T0 = 58.54;    %zacatecni �as
T1 = 59.5;    %konecny �as
Fsampling = 1/(dataset.time(2)-dataset.time(1));

angle_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,9);
angle_p = -(angle_p-mean(angle_p))*2*pi/8192; % �hel kmit�n� kyvadla

% poloha kyvadla
m1_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,2)/8192*0.040;
m2_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,4)/8192*0.040*2.5;


m1_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,7);
m2_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,8);
time = dataset.time(Fsampling*T0:Fsampling*T1);

p_p = m1_p; % pozice voz�ku
l_p = +m1_p-m2_p+3.7418e+04/8192*0.040*2.5; % d�lka z�v�su
% sni�ov�n� vzorkovac� frekvence - pokus o sn�en� �umu p�i derivac�ch
downrate = 1;
Fsampling = Fsampling/downrate; 
angle_p=subsamplingAverage(angle_p,downrate);
p_p=subsamplingAverage(p_p,downrate);
l_p=subsamplingAverage(l_p,downrate);
m1_c=subsamplingAverage(m1_c,downrate);
m2_c=subsamplingAverage(m2_c,downrate);
time=subsamplingAverage(time,downrate);

% Derivace a filtrov�n� a osek�n� okraj� m��en� kv�li filtr�m a derivac�m - �ekl bych, �e funguje dob�e
sampleDelay = 1;
angle_v = diff(angle_p)*Fsampling;
angle_a = diff(angle_v)*Fsampling;
l_v =  diff(l_p)*Fsampling;
l_a =  diff(l_v)*Fsampling;
p_v = diff(p_p)*Fsampling;
p_a = diff(p_v)*Fsampling;
angle_a = angle_a(sampleDelay:end);
angle_v = angle_v(sampleDelay:end-1);
angle_p = angle_p(sampleDelay:end-2);

p_a = p_a(sampleDelay:end);
p_v = p_v(sampleDelay+1:end);
p_p = p_p(sampleDelay+2:end);

l_p = l_p(sampleDelay+2:end);
l_v = l_v(sampleDelay+1:end);
l_a = l_a(sampleDelay:end);

m1_c = m1_c(sampleDelay+2:end);
m2_c = m2_c(sampleDelay+2:end);
time = time(sampleDelay+2:end);

T0 = 2.438;
T1 = 5.95;

%%
time = time - min(time);
figure(1); hold off
plot(time,p_p)
figure(2); hold off
plot(time,p_v)


syms dx(t) x(t)
 % m_v*diff(p(t), t, t) == t_m2*in2(t) - 1.0*b_v*diff(p(t), t) - 1.0*b_vs*sign(diff(p(t), t))
% b_v = 0.85/m_v ; b_vs = -1.9/m_v;
% dx = dx
m_k= 0.101;
m_vozitko= 2.3; %0.3 % ciste vozitko 0.4 kg %motor 2.4 kg
m_motor = 0.32; % 0.7
%ddx = -2*dx/m_vozitko -2.35*sign(dx)/m_vozitko-9.81*m_k/m_vozitko;
ddx = -4.2*dx/m_vozitko -3.5*sign(dx)/m_vozitko%-9.81*m_k/m_vozitko;
ddx2 = -8.2*dx/m_vozitko 
f = [dx;ddx];
f2 = [dx;ddx2];
% cond1 = x(0) == p_p(1);
% cond2 = Dx(0) == p_v(1);
% conds = [cond1 cond2];
odefun = odeFunction(f,[x(t) dx(t)]);
odefun2 = odeFunction(f2,[x(t) dx(t)]);
tspan = [min(time) max(time)];
initConditions = [p_p(1);p_v(1)] ;
[tim,x] = ode45(odefun, tspan, initConditions);
[tim2,x2] = ode45(odefun2, tspan, initConditions);

%[t,y] = ode45(@(t, dx) f(t,), [0; 5], [p_p(1);p_v(1)]);

% ySol(t) = dsolve(ode,conds);
% ySol = simplify(ySol)
figure(1); hold on
plot(tim,x(:,1))
plot(tim2,x2(:,1))
legend("Nam��en� data","Prolo�en� k�ivka s konstatn�m tlumen�m","Prolo�en� k�ivka bez konstatn�ho tlumen�")
ylabel('$p\, [m]$','interpreter','latex'); xlabel("t [s]");grid on
figure(2); hold on
plot(tim,x(:,2))
plot(tim2,x2(:,2))
laby = ylabel('$ \dot{p}\, \left[ m s^{-1} \right] $','interpreter','latex'); xlabel("t [s]");grid on
legend("Nam��en� data","Prolo�en� k�ivka s konstatn�m tlumen�m","Prolo�en� k�ivka bez konstatn�ho tlumen�",'location','best')

% 
% saveas(figure(1),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafBVIdenP','epsc')
% saveas(figure(2),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafBVIdenV','epsc')

