I  = [2;3;4;5;6;7;8]
F = [1.5;2.5;3;3.5;4.1;5.2;5.5]*0.05
figure(123)   
scatter(I,F,25,'b','*') 
P = polyfit(I,F,1);
iplot = 0:0.01:max(I)+1;
yfit = P(1)*iplot+P(2);
hold on;
plot(iplot,yfit,'r-.'); hold off;
xlabel('I [A]'); ylabel('M [Nm]'); grid on; xlim([0,9]) 
saveas(figure(123),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafMerMotor','epsc')

%%
F = [0;0.019047982;0.034661082;0.06713633;0.134116528;0.152852248;0.165498859;0.179082256;0.198754762;0.221706019]
I  = [0;1;2;3;4;5;6;7;8;9]
figure(123)   
scatter(I,F,25,'b','*') 
P = polyfit(I,F,1);
iplot = 0:0.01:max(I)+1;
yfit = P(1)*iplot+P(2);
hold on;
plot(iplot,yfit,'r-.'); hold off;
xlabel('I [A]'); ylabel('M [Nm]'); grid on; xlim([0,9]) 
saveas(figure(123),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafMerMotor2','epsc')