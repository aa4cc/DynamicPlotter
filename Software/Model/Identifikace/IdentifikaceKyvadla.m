clear all, close all, clc
load('D:\Projekty\DynamicPlotter-mastervad\Software\Model\data09032020-2.mat')
Oscilace =[oscilace1, oscilace3, oscilace4, oscilace5, oscilace6, oscilace7, oscilace8, oscilace9, oscilace10, oscilace11, oscilace12] %oscilace2,
count = 11
Omegas = zeros(1,count);
Deltas = zeros(1,count);
Amps = zeros(1,count);
Offsets = zeros(1,count);
Lenghts = zeros(1,count);
for i = 1:count
    oscilace = Oscilace(i);
    Fsampling = 1/(oscilace.time(2)-oscilace.time(1));
    T0 = 20;
    T1 = 70;
    data = oscilace.signals.values(Fsampling*T0:Fsampling*T1,9)/8192*2*pi;
    data = data-mean(data)
    timeTemp = oscilace.time(Fsampling*T0:Fsampling*T1);
    [Omega,Beta,Amp,Offset,Omega0]=FitExpSin(timeTemp,data,i);
    Omegas(i) = Omega;
    Deltas(i) = Beta;
    Amps(i) = Amp;
    Offsets(i) = Offset;
    inLenghts(i) = oscilace.signals.values(end,3)
    
end
%%
Lenghts = -inLenghts 
Omegas0 = sqrt(Omegas.^2+Deltas.^2)
syms a k0
eq1 = sqrt(9.810/(a*(Lenghts(1)+k0))) == Omegas0(1)
eq2 = sqrt(9.810/(a*(Lenghts(10)+k0))) == Omegas(10)
sol = solve([eq1,eq2])
double(sol.a)
double(sol.k0)
Omegas0 = sqrt(Omegas.^2+Deltas.^2)
trueLenghts = double(sol.a)*(Lenghts+double(sol.k0))

figure(58)
hold off
scatter(trueLenghts,Omegas0,'filled')
hold on
plotLenghts= min(trueLenghts):0.01:max(trueLenghts);
f= sqrt(9.810./plotLenghts);
plot(plotLenghts,f);
ylabel('\omega_0 [rad \cdot s^-1]');xlabel('l [m]')
legend('nam��en� data', 'prolo�en� k�ivkou'); grid on

trueLenghts = double(sol.a)*(Lenghts+double(sol.k0))
figure(55)
hold off;
scatter(trueLenghts,Deltas,'filled');hold on;
ylabel('\delta');xlabel('l [m]'); grid on



figure(59)
hold off;
scatter(trueLenghts,Deltas*(2*0.101),'filled')
hold on;
ylabel('b_k');xlabel('l [m]'); grid on
yline(mean(Deltas*(2*0.101)),'--m')
yline(mean(0.004),'--r')
legend('nam��en� data','st�edn� hodnota','zvolen� hodnota')

saveas(figure(55),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafDELTAnaL','epsc')
saveas(figure(58),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafOMEGA0naL','epsc')
saveas(figure(59),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafBKnaL','epsc')