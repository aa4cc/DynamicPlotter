clear all,clc
load('pady.mat')
%konecna rychlost asi 0.1372 0.1388
dataset=pad1;
T0 = 3.5;    %zacatecni �as
T1 = 5.5;    %konecny �as

T0 = 2.8038;
T1 = 5.4;

Fsampling = 1/(dataset.time(2)-dataset.time(1));

angle_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,9);
angle_p = -(angle_p-mean(angle_p))*2*pi/8192; % �hel kmit�n� kyvadla

% poloha kyvadla
m1_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,2)/8192*0.040;
m2_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,4)/8192*0.040*2.5;


m1_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,7);
m2_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,8);
time = dataset.time(Fsampling*T0:Fsampling*T1);

p_p = m1_p; % pozice voz�ku
l_p = +m1_p-m2_p+3.7418e+04/8192*0.040*2.5; % d�lka z�v�su
% sni�ov�n� vzorkovac� frekvence - pokus o sn�en� �umu p�i derivac�ch
downrate = 1;
Fsampling = Fsampling/downrate; 
angle_p=subsamplingAverage(angle_p,downrate);
p_p=subsamplingAverage(p_p,downrate);
l_p=subsamplingAverage(l_p,downrate);
m1_c=subsamplingAverage(m1_c,downrate);
m2_c=subsamplingAverage(m2_c,downrate);
time=subsamplingAverage(time,downrate);

% Derivace a filtrov�n� a osek�n� okraj� m��en� kv�li filtr�m a derivac�m - �ekl bych, �e funguje dob�e
sampleDelay = 1;
angle_v = diff(angle_p)*Fsampling;
angle_a = diff(angle_v)*Fsampling;
l_v =  diff(l_p)*Fsampling;
l_a =  diff(l_v)*Fsampling;
p_v = diff(p_p)*Fsampling;
p_a = diff(p_v)*Fsampling;
angle_a = angle_a(sampleDelay:end);
angle_v = angle_v(sampleDelay:end-1);
angle_p = angle_p(sampleDelay:end-2);

p_a = p_a(sampleDelay:end);
p_v = p_v(sampleDelay+1:end);
p_p = p_p(sampleDelay+2:end);

l_p = l_p(sampleDelay+2:end);
l_v = l_v(sampleDelay+1:end);
l_a = l_a(sampleDelay:end);

m1_c = m1_c(sampleDelay+2:end);
m2_c = m2_c(sampleDelay+2:end);
time = time(sampleDelay+2:end);

T0 = 2.438;
T1 = 5.95;

time = time - min(time)
figure(1); hold off
plot(time,l_p)
figure(2); hold off
plot(time,l_v)


syms dx(t) x(t)
 % m_k*diff(l(t), t, t) - 9.8*m_k + m_m*diff(l(t), t, t) == t_m1*in1(t) - 1.0*b_m*diff(l(t), t) - 1.0*b_ms*sign(diff(l(t), t))
% b_m = 1.8000/(m_k+m_m) ; b_ms = 0.7400/(m_k+m_m);
% dx = dx
m_k= 0.101;
m_m = 0.42
b_m = 3*m_m
b_ms = (-0.1388 * b_m  + 0.9898)
%ddx = -b_m*dx -b_ms*sign(dx)+9.8*m_k;
ddx = -b_m*dx/m_m -b_ms*sign(dx)/m_m+9.8*m_k/m_m;
ddx2 = -7*dx/m_m +9.8*m_k/m_m;
f = [dx;ddx]
f2 = [dx;ddx2]
% cond1 = x(0) == p_p(1);
% cond2 = Dx(0) == p_v(1);
% conds = [cond1 cond2];
odefun = odeFunction(f,[x(t) dx(t)]);
odefun2 = odeFunction(f2,[x(t) dx(t)]);
tspan = [min(time) max(time)];
initConditions = [l_p(1);l_v(1)] ;
[tim,x] = ode45(odefun, tspan, initConditions);
[tim2,x2] = ode45(odefun2, tspan, initConditions);
%[t,y] = ode45(@(t, dx) f(t,), [0; 5], [p_p(1);p_v(1)]);

% ySol(t) = dsolve(ode,conds);
% ySol = simplify(ySol)
figure(1); hold on
plot(tim,x(:,1))
plot(tim2,x2(:,1))
legend("Nam��en� data","Prolo�en� k�ivka s konstatn�m tlumen�m","Prolo�en� k�ivka bez konstatn�ho tlumen�",'location','best')
laby = ylabel('$L \, \left[ m \right] $','interpreter','latex'); xlabel("t [s]");grid on
figure(2); hold on
plot(tim,x(:,2))
plot(tim2,x2(:,2))
laby = ylabel('$ \dot{L}\, \left[ m s^{-1} \right] $','interpreter','latex'); xlabel("t [s]");grid on
legend("Nam��en� data","Prolo�en� k�ivka s konstatn�m tlumen�m","Prolo�en� k�ivka bez konstatn�ho tlumen�",'location','best')

% saveas(figure(1),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafBMIdenP','epsc')
% saveas(figure(2),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafBMIdenV','epsc')
% % 
