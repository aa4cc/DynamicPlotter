 %clear all,close all; clc
% skript na odvozeni rovnic matematick�ho modelu
OdvozeniMatModel
%%
tic;
Setpoints = [[0, -0.5];  [0.5, -0.6];  [0, -0.8];  [-0.2, -0.6]; [0, -0.5]]

NumSetpoints = length(Setpoints)
krokuNaSpojnici = 35;

%omezen� maxim�ln� vzd�lonosti od jednotliv�ch bod� a  spojnic t�chto 
MaxVzdalenostSpojnice = 0.03
MaxVzdalenostBody = 0.01 

Tf = 10;

N = krokuNaSpojnici*(NumSetpoints-1)
method = 'euler';
% method = 'rk4';
method = 'collocation';

umin = -Inf;
umin = -5;
umax = -umin;
DphiOMax = 20; %3




% Inicializace casadi
opti = casadi.Opti();

% define decision variables
X = opti.variable(6,N+1); % stavy
LO = X(1, :);
DLO = X(2, :);
phiO = X(3, :);
DphiO = X(4, :);
pO = X(5, :);
DpO = X(6, :);
xO = (LO-pO).*sin(phiO)+pO
yO = -(LO-pO).*cos(phiO)
vxO = DpO + sin(phiO).*(LO - DpO) + cos(phiO).*(LO - pO).*DphiO
vyO = -(cos(phiO).*(LO - DpO) - sin(phiO).*(LO - pO).*DphiO)


U = opti.variable(2,N); % controls
timepart = opti.variable(1,NumSetpoints-1)  % d�lky �asov�ho kroku na spojnic�ch jednostliv�ch bod�
opti.subject_to(0.0001<=timepart); % Omezen� minima pro numerickou stabilitu

% omezen� maxim�ln�ho ak�n�ho z�sahu (zrychlen�)
opti.subject_to(umin<=U<=umax);
% omezen� maxim�ln�ho �hlov� rychlosti k�v�n� z�sahu (zrychlen�)
opti.subject_to(-DphiOMax<=DphiO<=DphiOMax);

%Omezen� maxim�ln� vzd�lenosti od spojnice bod� trajektorie
for index  = 1:NumSetpoints-1
    Origin = Setpoints(index,:)
    Goal = Setpoints(index+1,:)
    Vector = Goal-Origin
    Projektor = eye(2) - (Vector'*Vector/(Vector*Vector')) %projektor na prostor kolm� na p��mku Origin - Goal
    for index2=(index-1)*krokuNaSpojnici+1:index*krokuNaSpojnici+1
        bodTrajektorie = [xO(index2); yO(index2)];
        vzdalenostPrimka = sum((Projektor*(bodTrajektorie-Origin')).^2);
        opti.subject_to(vzdalenostPrimka<=MaxVzdalenostSpojnice^2);
   end
end        


%Omezeni trajektorie kolem bod� trajektorie
for index  = 2:NumSetpoints-1
    dx = xO(krokuNaSpojnici*(index-1))-Setpoints(index,1)
    dy = yO(krokuNaSpojnici*(index-1))-Setpoints(index,2)
    opti.subject_to(dx^2+dy^2<= MaxVzdalenostBody^2)
 end

%vyj�d�en� integr�lu/sumy vstup� (zrychlen�) p�es �as trajektorie
akcniZasahInt = 0;
for index=1:NumSetpoints-1
   akcniZasahInt = akcniZasahInt+timepart(:,index)* sum(sum(U(:,(index-1)*krokuNaSpojnici+1:index*krokuNaSpojnici).*U(:,(index-1)*krokuNaSpojnici+1:index*krokuNaSpojnici)));
end

%optimalizovan� krit�rium
opti.minimize(krokuNaSpojnici*sum(timepart)+0.01*akcniZasahInt);%0.0005

f = @(x,u) simple_dynPlotter_ode_gen(x, u); % dx/dt = f(x,u)


% Omezen� dan� dynamikou syst�mu a r�zn� metody diskretizace
switch lower(method)
    case 'euler'
        % ---- Forward Euler --------
        for index=1:NumSetpoints-1  
            dt = timepart(:,index);
            for index2=((index-1)*krokuNaSpojnici+1):(index*krokuNaSpojnici) % loop over control intervals
                x_next = X(:,index2) + dt*f(X(:,index2), U(:,index2));
                opti.subject_to(X(:,index2+1)==x_next); % close the gaps
            end
        end
    case 'rk4'
        % ---- RK4 --------
        for index=1:NumSetpoints-1 
            dt = timepart(:,index);
            for index2=(index-1)*krokuNaSpojnici+1:index*krokuNaSpojnici % loop over control intervals
                k1 = f(X(:,index2),         U(:,index2));
                k2 = f(X(:,index2)+dt/2*k1, U(:,index2));
                k3 = f(X(:,index2)+dt/2*k2, U(:,index2));
                k4 = f(X(:,index2)+dt*k3,   U(:,index2));
                x_next = X(:,index2) + dt/6*(k1+2*k2+2*k3+k4);
                opti.subject_to(X(:,index2+1)==x_next); % close the gaps
            end
        end
        case 'collocation' 
        % ---- Collocation --------
        for index=1:NumSetpoints-1 
            dt = timepart(:,index);
            for index2=(index-1)*krokuNaSpojnici+1:index*krokuNaSpojnici % loop over control intervals
                u1 = U(:,index2);
                if index2 ~=  N
                    u2 = U(:,index2+1);
                else
                    u2 = U(:,index2);
                end
                x1 = X(:,index2);
                x2 = X(:,index2+1);
                f1 = f(x1, u1);
                f2 = f(x2, u2);
                xc = (x1 + x2)/2 + dt*(f1 - f2)/8;
                fc = f(xc, (u1 + u2)/2);
                opti.subject_to(dt*fc + 3*(x1-x2)/2 + dt*(f1+f2)/4 == 0); % close the gaps 
            end
        end
    otherwise
        error('Unknown method was chosen!');
end

% Initial conditions
opti.subject_to(pO(1)==0);   % start at position 0 ...
opti.subject_to(DpO(1)==0); % ... from stand-still 
opti.subject_to(LO(1)==abs(Setpoints(1,2)));   % start at position 0 ...
opti.subject_to(DLO(1)==0); % ... from stand-still
opti.subject_to(phiO(1)==0);   % start at position 0 ...
opti.subject_to(DphiO(1)==0); % ... from stand-still




% Final-time constraints

opti.subject_to(pO(end)==0);   % start at position 0 ...
opti.subject_to(DpO(end)==0); % ... from stand-still 
opti.subject_to(LO(end)==abs(Setpoints(end,2)));   % start at position 0 ...
opti.subject_to(DLO(end)==0); % ... from stand-still
opti.subject_to(phiO(end)==0);   % start at position 0 ...
opti.subject_to(DphiO(end)==0); % ... from stand-still



% Initialize decision variables
opti.set_initial(U, 0);
opti.set_initial(pO, 0);
opti.set_initial(DpO, 0);
opti.set_initial(LO, abs(Setpoints(1,2)));
opti.set_initial(DLO, 0);
opti.set_initial(phiO, 0);
opti.set_initial(DphiO, 0);
opti.set_initial(timepart, 0.1);

% P��kaz k �e�en�
opti.solver('ipopt'); % use IPOPT solver

sol = opti.solve();
toc; % konec m��en� �asu v�po�tu
%%
x_nlp = sol.value(X)';
u_nlp = sol.value(U)';
dt = sol.value(timepart(:,1))';
switch lower(method)
    case 'collocation'
        Tset = [0];
        t_states = (0:krokuNaSpojnici)*dt % �as pro stavy a n�sledn� porovn�n� s v�sledky simulace
        Tset= [Tset,max(t_states)];
        lastdt = dt
        for k = (2:(NumSetpoints-1))
            dt = sol.value(timepart(:,k))';
            t_states = [t_states, (1:krokuNaSpojnici)*dt + max(t_states)];
            Tset= [Tset,max(t_states)];
            lastdt = dt
        end
        t_UInterp =  t_states;
    otherwise
        Tset = [0];
        t_states = (0:krokuNaSpojnici)*dt % �as pro stavy a n�sledn� porovn�n� s v�sledky simulace
        t_UInterp = (0:krokuNaSpojnici)*dt+dt/2 % �as pro naslednou interpolaci ak�n�ho z�sahu (posun o polovinu kroku)
        Tset= [Tset,max(t_states)]; 
        lastdt = dt
        for k = (2:(NumSetpoints-1))
            dt = sol.value(timepart(:,k))';
            t_states = [t_states, (1:krokuNaSpojnici)*dt + max(t_states)];
            t_UInterp = [t_UInterp, (1:krokuNaSpojnici)*dt + max(t_UInterp)+dt/2-0.5*lastdt];
            Tset= [Tset,max(t_states)];
            lastdt = dt
        end
end
% Stavy z�skan� simulac� 
LOO = x_nlp(:, 1);  % odvinut� lanko
DLOO = x_nlp(:, 2);
phiOO = x_nlp(:, 3); % �hel v�kyvu 
DphiOO = x_nlp(:, 4);
pOO = x_nlp(:, 5); % poloha voz�ku 
DpOO = x_nlp(:, 6);

xOO = (LOO-pOO).*sin(phiOO)+pOO; % x poloha kyvadla
yOO = -(LOO-pOO).*cos(phiOO); % y poloha kyvadla



%% Simulace syst�mu pro vypo��tan� vstupy (zrychlen�)

a_cart = @(t) interp1(t_UInterp(1:end-1),u_nlp(:,1),t,'linear','extrap') %,'spline''pchip'
a_line = @(t) interp1(t_UInterp(1:end-1),u_nlp(:,2),t,'linear','extrap');%,'spline'

endtime =max(t_UInterp(1:end-1))
x0 = [LOO(1); DLOO(1);    phiOO(1);     DphiOO(1);  pOO(1);   DpOO(1)];

% Simulation
[tsim, X] = ode23s(@(t, x) simple_dynPlotter_ode_gen(x,[a_cart(t); a_line(t)]), [0; endtime], x0)
%%

% Interpolace pro vizualizaci
t_plot = min(t_states):1/200:max(t_states);
X2 = interp1(tsim,X,t_plot);
X3 = interp1(t_states,x_nlp,t_plot);

Xpen = X2(:,5)+(X2(:,1)-X2(:,5)).*sin(X2(:,3));
Ypen = -(X2(:,1)-X2(:,5)).*cos(X2(:,3));

Xpen2 = X3(:,5)+(X3(:,1)-X3(:,5)).*sin(X3(:,3));
Ypen2 = -(X3(:,1)-X3(:,5)).*cos(X3(:,3));

  % vizualizace
figure(1)
for cnt = 1:numel(t_plot)
    xtemp = X2(cnt,5);
    %phi = X2(cnt,3);
    fill([-100 100 100 -100]*1e-3+xtemp,[0 0 100 100]*1e-3,[.5 .5 .5]); % vozik
    hold on;
    plot([-100, xtemp,Xpen(cnt)],[0,0,Ypen(cnt)],'o-');                         % zaves
    plot([-100, X3(cnt,5),Xpen2(cnt)],[0,0,Ypen2(cnt)],'o-');                         % zaves
    plot(Xpen(1:cnt),Ypen(1:cnt),'-');                              % trajektorie    
    plot(Xpen2(1:cnt),Ypen2(1:cnt),'-');                              % trajektorie
    plot(Setpoints(:,1),Setpoints(:,2))
    hold off;
    axis equal;
    axis([-1,1,-1.0,0.2]);
    set(gca, 'XDir', 'reverse');
%     set(gca, 'YDir', 'reverse');
    pause(0.00001);
end
%% plot stav�
figure(2)
subplot(4,2,1)
hold off
plot(t_plot,(X2(:,1)-X2(:,5)))
hold on; plot(t_UInterp',LOO-pOO)
ylabel('l(t) [m]');xlim([0,endtime])

subplot(4,2,2)
hold off
plot(t_plot,(X2(:,2)-1*X2(:,6)))
hold on; plot(t_UInterp',DLOO-DpOO)
ylabel('dl(t) [m/s]');xlim([0,endtime])


subplot(4,2,3)
hold off;plot(t_plot,X2(:,5))
hold on;plot(t_UInterp',pOO)
ylabel('p(t) [m]');xlim([0,max(t_plot)])

subplot(4,2,4)
hold off;plot(t_plot,X2(:,6))
hold on;plot(t_UInterp',DpOO)
ylabel('dp(t) [m/s]');xlim([0,max(t_plot)])

subplot(4,2,5)
hold off; plot(t_plot,X2(:,3))
hold on; plot(t_UInterp',phiOO)
ylabel('\phi(t) [rad]'); legend("model","optimalizace");xlim([0,max(t_plot)])

subplot(4,2,6)
hold off;plot(t_plot,X2(:,4))
hold on;plot(t_UInterp',DphiOO)
xlabel('t [s]'); ylabel('\omega(t) [rad/s]');xlim([0,max(t_plot)])

subplot(4,2,7)
hold off;plot(t_plot,X2(:,1))
hold on;plot(t_UInterp',LOO)
xlabel('t [s]'); ylabel('L(t)');xlim([0,max(t_plot)])

subplot(4,2,8)
hold off;plot(t_plot,X2(:,2))
hold on;plot(t_UInterp',DLOO)
xlabel('t [s]'); ylabel('DL(t)');xlim([0,max(t_plot)])


figure(15)
hold off
stairs(t_states(1:end-1), u_nlp);
hold on
plot(t_plot, a_cart(t_plot));
plot(t_plot, a_line(t_plot));
legend("opti cart","opti line","inter line","interp cart")



figure(18)
hold off
stairs(t_states(1:end-1), u_nlp(:,2)-u_nlp(:,1));
hold on
stairs(t_states, 4*cos( x_nlp(:,3)));


figure(86)
hold on; grid on;
plot(Xpen(1:end),Ypen(1:end),'-');                              % trajektorie    
plot(Xpen2(1:end),Ypen2(1:end),'-');   
legend('Euler simulace','Euler pl�nov�n�','Runge-Kutte simulace','Runge-Kutte pl�nov�n�','Hermit-Simpson simulace','Hermit-Simpson pl�nov�n�','location','best')
xlabel('x [m]');ylabel('y [m]')
set(gca, 'XDir', 'reverse');
% set(gca, 'YDir', 'reverse');

figure(87)
hold on
plot(t_plot,X2(:,5))
legend('Euler','Runge-Kutte','Hermit-Simpson','location','best')
xlabel('t [s]');ylabel('p [m]');grid on


figure(88)
hold on
plot(t_plot,X2(:,1))
legend('Euler','Runge-Kutte','Hermit-Simpson','location','best')
xlabel('t [s]');ylabel('L [m]'); grid on

%%  P�edov pro model v simulinku
accelerations.time = t_plot';
accelerations.signals.values = [a_cart(t_plot)',a_line(t_plot)'];
accelerations.signals.dimensions =2;

velocities.time = t_states';
velocities.signals.values = [DpOO,DLOO];
velocities.signals.dimensions =2;

angles.time = t_states';
angles.signals.values = [phiOO];
angles.signals.dimensions =1;

RopeLen.time = t_states';
RopeLen.signals.values = [LOO];
RopeLen.signals.dimensions =1;

CartPos.time = t_states';
CartPos.signals.values = [pOO];
CartPos.signals.dimensions =1;

GoalStates.time =  t_states';
GoalStates.signals.values = [x_nlp];
GoalStates.signals.dimensions =6;

m_zavazi= 0.101;
t_mL = 1.92; % lane (0.033/(0.04/2/pi)/2.5  = 2.07) dle prvniho mereni % druhe mereni 1.66
t_mP = 4.25; % cart (0.033/(0.04/2/pi)  = 5.18) dle prvniho mereni % druhe mereni 4.15
m_vozitko= 2.3; %0.3 % ciste vozitko 0.4 kg %motor 2.4 kg
m_motor = 0.32; % 0.7
% b_v = 0.85/m_v ; b_vs = 1.9/m_v; viz vozik % % b_m = 1.8000/(m_k+m_m) ; b_ms = 0.7400/(m_k+m_m);;
 prms = [m_zavazi;   m_vozitko;  m_motor   ;  0.040*m_zavazi;  3.10000;   9.0;    t_mL;    t_mP;   0 ; 0];
%       [m_k;          m_v;            m_m ;            b_k;     b_m;    b_v;    t_mL;    t_mP;  b_vs ;       b_ms];
