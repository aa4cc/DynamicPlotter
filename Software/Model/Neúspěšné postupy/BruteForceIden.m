clear all
clc
%close all
syms m_k m_v m_m b_m b_v beta t_m1 t_m2 
syms l(t) varphi(t) p(t) in1(t) in2(t)
x(t) = (l(t))*sin(varphi(t))+p(t)
y(t) = (l(t))*cos(varphi(t))

vx(t) = diff(x(t),t)
vy(t) = diff(y(t),t)
vp(t) = diff(p(t),t)
vl(t) = diff(l(t),t)

 
TF = 1/2 *m_k* (vx(t)^2 + vy(t)^2) + 1/2 * (m_v+m_m) * vp(t)^2 +  1/2 * m_m * (vl(t)+vp(t))^2 

VF = - 9.81*m_k*y(t)

LF = TF-VF

T = 1/2 * (vx(t)^2 + vy(t)^2) %+ 1/2 * (m_v+m_m) * vp(t)^2 +  1/2 * m_m * (vl(t)+vp(t))^2 

V = - 9.81*y(t)

L = T-V

LeqrA = functionalDerivative(LF,[varphi l p])
LeqrPhi = functionalDerivative(L,varphi)
LeqrL = functionalDerivative(L,l)
LeqrP = functionalDerivative(L,p)

dis = [-beta*diff(varphi,t)*(l(t))^2]
disA = [-beta*diff(varphi,t)*(l(t))^2; -b_m*diff(l,t)+t_m1*in1; -(b_m+b_v)*diff(p,t)+t_m2*in2]
disA = [-beta*diff(varphi,t)*(l(t))^2; +t_m1*in1; +t_m2*in2]
disPhi = [-beta*diff(varphi,t)*(l(t))^2];
disL = [ +t_m1*in1];
disP = [ +t_m2*in2];
LeqrDissA = LeqrA == disA
LeqrDissPhi = LeqrPhi == disPhi
LeqrDissL = LeqrL == disL
LeqrDissP = LeqrP == disP
syms phi(t)
LeqrDissL = subs(LeqrDissL,{varphi(t)},{0})
LeqrDissP = subs(LeqrDissP,{varphi(t)},{0})

latex(simplify(LeqrDissL))
latex(simplify(LeqrDissP))

%%
load('D:\Projekty\DynamicPlotter-mastervad\Software\data09032020-2.mat')
DataSet=oscilace11
T0 = 10;
T1 = 50;
Fsampling = 1/(DataSet.time(2)-DataSet.time(1));

downrate = 1;


angle_p = DataSet.signals.values(Fsampling*T0:Fsampling*T1,9);
angle_p = -(angle_p-mean(angle_p))*2*pi/8192;

m1_p = DataSet.signals.values(Fsampling*T0:Fsampling*T1,2)/8192*0.040;
m2_p = DataSet.signals.values(Fsampling*T0:Fsampling*T1,4)/8192*0.040*2.5;

m1_c = DataSet.signals.values(Fsampling*T0:Fsampling*T1,7);
m2_c = DataSet.signals.values(Fsampling*T0:Fsampling*T1,8);
time = DataSet.time(Fsampling*T0:Fsampling*T1);

Fsampling = Fsampling/downrate; 
angle_p=subsamplingAverage(angle_p,downrate);
m1_p=subsamplingAverage(m1_p,downrate);
m2_p=subsamplingAverage(m2_p,downrate);
m1_c=subsamplingAverage(m1_c,downrate);
m2_c=subsamplingAverage(m2_c,downrate);
time=subsamplingAverage(time,downrate);

p_p = m1_p;
l_p = +m1_p-m2_p+3.7418e+04/8192*0.040*2.5;


%%
sampleDelay = 30;
angle_v = [diff(angle_p)]*Fsampling;
angle_a = [diff(angle_v)]*Fsampling;
angle_a = angle_a(sampleDelay:end);
angle_v = angle_v(sampleDelay:end-1);
angle_p = angle_p(sampleDelay:end-2);
p_v = diff(p_p)*Fsampling;
p_a = diff(p_v)*Fsampling;
p_a = p_a(sampleDelay:end);
p_v = p_v(sampleDelay+1:end);
p_p = p_p(sampleDelay+2:end);

%%
l_v =  diff(l_p)*Fsampling;
l_a =  diff(l_v)*Fsampling;


l_p = l_p(sampleDelay+2:end);
l_v = l_v(sampleDelay+1:end);
l_a = l_a(sampleDelay:end);

m1_c = m1_c(sampleDelay+2:end);
m2_c = m2_c(sampleDelay+2:end);
time = time(sampleDelay+2:end);
%%
figure(4)
hold off
plot(time,l_p,time,l_v,time,l_a,time,m2_c)
legend('l_p','l_v','l_a','m2_c')

figure(2)
hold off
plot(time,p_p,time,p_v,time,p_a,time,m1_c)
legend('p_p','p_v','p_a','m1_c')

figure(3)
cla reset
hold off
%yyaxis left
plot(time,angle_p,time,angle_v)
hold on
yyaxis right
plot(time,angle_a)
legend('angle_p','angle_v','angle_a')

%% State equation
% We now need to extract the highest, that is, the second derivative of phi
% and then we are immediately ready to write down the state equation. It
% appears that even solving the equation is not possible for functions.
% Once again the clumsy substition trick finds its use here. But there is a
% neater way using a higher level function, see the next section.

syms dphi2dt2

Leqrc = subs(LeqrDissPhi,diff(varphi(t),t,t),dphi2dt2)
dphi2dt2_sol = solve(Leqrc,dphi2dt2)

%% 
% We now introduce a new variable - the angular velocity $\omega(t)$ 

syms omega(t)

%%
% and the right hand side of one of the two state equations is

f2 = subs(dphi2dt2_sol,diff(varphi(t),t),omega(t))

%%
% and the right hand side of the other state equation is

f1 = omega(t)

%% 
% Combining the two into a single vector function we get

f = [f1;f2]

% Alternative procedure for converting Lagrange equation to state equations using some Symbolic Tbx functionalities
% There are two useful functions in Symbolic Toolbox, namely
% <matlab:doc('reduceDifferentialOrder') reduceDifferentialOrder>  and <matlab:doc('massMatrixForm') massMatrixForm>.

[eqs,vars] = reduceDifferentialOrder(LeqrPhi,varphi(t))
[M,F] = massMatrixForm(eqs,vars)
fs = M\F

 
% time = time - min(time)
% syms t
% %l(t) = 1;  % This is here just for checking the model in case of constant lenght of the pendulum
% 
% [Omega,Beta,Amp,Offset,Omega0]=FitExpSin(time,l_p,5);
% 
% 
% l(t) = Offset+Amp*sin(Omega*t+Omega0)
% figure(15)
% hold off; plot(time,l_p)
% hold on;  plot(time,l(time))
% [Omega,Beta,Amp,Offset,Omega0]=FitExpSin(time,p_p,6);
% p(t) = Offset+Amp*sin(Omega*t+Omega0)
% figure(16)
% hold off; plot(time,p_p)
% hold on;  plot(time,p(time))
% %p(t) = interp1(time,p_p,t,'spline');
% beta = -0.050
% f = subs(f)
% 
% odefun = odeFunction(f,[varphi(t) omega(t)]);
% 
% % Setting the simulation parameters and calling an ODE solver
% tspan = [min(time) max(time)];
% initConditions = [angle_p(1); angle_v(1);];
% 
% [tim,x] = ode45(odefun, tspan, initConditions);
% 
% % Plotting the solutions
% figure(1)
% subplot(4,1,1)
% hold off
% plot(tim,l(tim))
% hold on
% plot(time,l_p)
% ylabel('l(t) [m]')
% 
% subplot(4,1,2)
% hold off
% plot(tim,p(tim))
% hold on
% plot(time,p_p)
% ylabel('p(t) [m]')
% 
% 
% subplot(4,1,3)
% hold off
% plot(tim,x(:,1))
% hold on
% plot(time,angle_p)
% ylabel('\phi(t) [rad]')
% legend("fit","data")
% 
% subplot(4,1,4)
% hold off
% plot(tim,x(:,2))
% hold on
% plot(time,angle_v)
% xlabel('t [s]')
% ylabel('\omega(t) [rad/s]')


%%
% 
% eqs = subs(LeqrDissA,...
%     {'m_v', 'm_m', 'b_m', 'b_v', 'beta', 't_m1', 't_m2'},...
%      {1    1    1    1    1      1     1}   )
% vars = [l(t), varphi(t), p(t)];
% [l(t), varphi(t), p(t) in1(t) in2(t)]
% 
% f = daeFunction(eqs, vars, in1(t), in2(t))

%%
% rov = sym2cell(LeqrDissA)
% syms dphi2dt2
% Mass  = zeros(3);
% for  i  = 1:3
%     
% end


eqns = [LeqrDissPhi;LeqrDissL;LeqrDissP]
syms m_v m_m b_m b_v beta t_m1 t_m2 
syms l(t) varphi(t) p(t) in1(t) in2(t)


vars = [l(t); varphi(t); p(t)]
origVars = length(vars)

M = incidenceMatrix(eqns,vars)

[eqns,vars] = reduceDifferentialOrder(eqns,vars)

isLowIndexDAE(eqns,vars)
[DAEs,DAEvars] = reduceDAEIndex(eqns,vars)
[DAEs,DAEvars] = reduceRedundancies(DAEs,DAEvars)
isLowIndexDAE(DAEs,DAEvars)
pDAEs = symvar(DAEs)
pDAEvars = symvar(DAEvars)
extraParams = setdiff(pDAEs,pDAEvars)


f = daeFunction(DAEs,DAEvars, beta, t_m1, t_m2, in1, in2);
m_v=1; m_m=1; b_m=1; b_v=1; beta=0.04; t_m1=1; t_m2=1;in1 = 2; in2=3;


F = @(t,Y,DY) f(t,Y, DY, beta, t_m1, t_m2, in1, in2);
isLowIndexDAE(DAEs,DAEvars)
y0est = zeros(6,1);
yp0est = zeros(6,1);
opt = odeset('RelTol', 10.0^(-7),'AbsTol',10.0^(-7));
[y0,yp0] = decic(F,0,y0est,[],yp0est,[])

%y0 = y0est;yp0=yp0est; 

[tSol,ySol] = ode15i(F,[0 0.5],y0,yp0,opt);
plot(tSol,ySol(:,1:origVars),'LineWidth',2)

for k = 1:origVars
  S{k} = char(DAEvars(k));
end

legend(S,'Location','Best')
grid on
