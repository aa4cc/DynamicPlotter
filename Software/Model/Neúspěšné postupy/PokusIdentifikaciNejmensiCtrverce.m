%p�vodn� skript poch�z� z MSD pana Hur�ka, ale byl v�razn� upraven


% Je jsou tu sestaven� matematick� model, pokus o ur�en� konstant p�es
% nejmen�� �verce a model, kter� bere polohy voz�ku a d�lku z�v�su jako
% vstup

%% 
clear all
clc
close all
%% Symbolic variables and functions
% m_k - hmotnost kyvadla 
% m_v - hmotnost voz�ku, m_m - setrva�nost motoru, 
% b_m - tlumen� motoru, b_v - tlumen� vz�tka,  b_k - tlumen� kyvadla 
% t_m1 - p�evodn� konstanta pro motor 1, t_m2 - p�evodn� konstanta pro motor 2 
syms m_k m_v m_m b_m b_v b_k t_m1 t_m2 b_vs b_ms


% l - delka z�v�su
% varphi - �hel kyvadla
% p - poloha voz�tka
% in - vstupn� proudy jednotliv�ch motor�
syms l(t) varphi(t) p(t) in1(t) in2(t)


%% Coordinates of the pendulum

x(t) = (l(t)-p(t))*sin(varphi(t))+p(t)
y(t) = (l(t)-p(t))*cos(varphi(t))

%% Computing the velocities

vx(t) = diff(x(t),t)
vy(t) = diff(y(t),t)
vp(t) = diff(p(t),t)
vl(t) = diff(l(t),t)


%% Lagrange equation
%       zavazi                        vozitko + jeho motor      motor lanka        
T = 1/2 *m_k* ((vx(t))^2 + vy(t)^2) + 1/2 * (m_v) * vp(t)^2 +  1/2 * m_m * vl(t)^2
V =  -9.81*m_k*y(t)
LF = T-V

LeqrA = -functionalDerivative(LF,[varphi l p])
LeqrPhi = -functionalDerivative(LF,varphi)
LeqrL = -functionalDerivative(LF,l)
LeqrP = -functionalDerivative(LF,p)

disA = [-b_k*diff(varphi,t)*(l(t))^2 ; -b_m*diff(l,t)+t_m1*in1 - b_ms*sigmoidd(diff(l,t)) ; -(b_v)*diff(p,t)+t_m2*in2- b_vs*sigmoidd(diff(p,t))]
disPhi = -b_k*diff(varphi,t)*(l(t))^2;
disL = -b_m*diff(l,t)-b_m*diff(p,t)+ +t_m1*in1;
disP = -(b_m+b_v)*diff(p,t)+t_m2*in2;
LeqrDissEqA = LeqrA == disA
LeqrDissA = LeqrA - disA
LeqrDissEqPhi = LeqrPhi == disPhi
LeqrDissEqL = LeqrL == disL
LeqrDissEqP = LeqrP == disP
LeqrDissPhi = LeqrPhi - disPhi
LeqrDissL = LeqrL - disL
LeqrDissP = LeqrP - disP

%% N�jak� �pravy a zjednodu�en� pro latex ... - ned�le�it�
LeqrDissLAprox = subs(LeqrDissEqL,{varphi(t)},{0})
LeqrDissPAprox = subs(LeqrDissEqP,{varphi(t)},{0})
LeqrDissEqPhiAprox = subs(LeqrDissEqPhi,{p(t)},{0})
latex(simplify(LeqrDissEqA))
latex(simplify(subs(LeqrDissEqA,{'m_k','m_v', 'm_m', 'b_m', 'b_k'},{1 0 0 0 0})))

latex(simplify([LeqrDissEqPhiAprox;LeqrDissLAprox;LeqrDissPAprox]))
latex(simplify(subs([LeqrDissEqPhiAprox;LeqrDissLAprox;LeqrDissPAprox],{'m_k','m_v', 'm_m', 'b_m', 'b_v'},{1 0 0 0 0})))


%% Nacten� dat a p�e�k�lov�n� z hodnot enkoder� na re�ln� hondnoty - bezprobl�m� funguje
load('data09032020-2.mat')

dataset=kmity2
T0 = 10.5;    %zacatecni �as
T1 = 25;    %konecny �as
Fsampling = 1/(dataset.time(2)-dataset.time(1));




angle_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,9);
angle_p = (angle_p-mean(angle_p))*2*pi/8192; % �hel kmit�n� kyvadla

% poloha kyvadla
m1_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,2)/8192*0.040;
m2_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,4)/8192*0.040*2.5;

m1_v = dataset.signals.values(Fsampling*T0:Fsampling*T1,5)/8192*0.040;
m2_v = dataset.signals.values(Fsampling*T0:Fsampling*T1,6)/8192*0.040*2.5;


m1_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,7);
m2_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,8);
time = dataset.time(Fsampling*T0:Fsampling*T1);

p_p = -m1_p; % pozice voz�ku
l_p = +m1_p-m2_p+3.7418e+04/8192*0.040*2.5; % d�lka z�v�su
p_v = -m1_v; % pozice voz�ku
l_v = +m1_v-m2_v+3.7418e+04/8192*0.040*2.5; % d�lka z�v�su
%% sni�ov�n� vzorkovac� frekvence - pokus o sn�en� �umu p�i derivac�ch
downrate = 5;
Fsampling = Fsampling/downrate; 
angle_p=subsamplingAverage(angle_p,downrate);
p_p=subsamplingAverage(p_p,downrate);
l_p=subsamplingAverage(l_p,downrate);
p_v=subsamplingAverage(p_v,downrate);
l_v=subsamplingAverage(l_v,downrate);
m1_c=subsamplingAverage(m1_c,downrate);
m2_c=subsamplingAverage(m2_c,downrate);
time=subsamplingAverage(time,downrate);
%% n�vrh doln� propusti pro filtrov�n�
% rng default
% fc = 5;
% Wn = (2/Fsampling)*fc;
% b = fir1(20,Wn,'low',kaiser(21,3));
% fvtool(b,1,'Fs',Fsampling)


%% Derivace a filtrov�n� a osek�n� okraj� m��en� kv�li filtr�m a derivac�m - �ekl bych, �e funguje dob�e
sampleDelay = 30;
angle_v = diff(angle_p)*Fsampling;
angle_a = diff(angle_v)*Fsampling;
%l_v =  diff(l_p)*Fsampling;
l_a =  diff(l_v)*Fsampling;
%p_v = diff(p_p)*Fsampling;
p_a = diff(p_v)*Fsampling;

%  filtrov�n�
% angle_p = filter(b,1,angle_p);
% angle_v = filter(b,1,angle_v);
% angle_a = filter(b,1,angle_a);
% p_p = filter(b,1,p_p);
% p_v = filter(b,1,p_v);
% p_a = filter(b,1,p_a);
% l_p = filter(b,1,l_p);
% l_v = filter(b,1,l_v);
% l_a = filter(b,1,l_a);
% m1_c = filter(b,1,m1_c);
% m2_c = filter(b,1,m2_c);


angle_a = angle_a(sampleDelay:end);
angle_v = angle_v(sampleDelay+1:end);
angle_p = angle_p(sampleDelay+2:end);

p_p = p_p(sampleDelay+2:end);
p_v = p_v(sampleDelay+2:end);
p_a = p_a(sampleDelay+1:end);

l_p = l_p(sampleDelay+2:end);
l_v = l_v(sampleDelay+2:end);
l_a = l_a(sampleDelay+1:end);

m1_c = m1_c(sampleDelay+2:end);
m2_c = m2_c(sampleDelay+2:end);
time = time(sampleDelay+2:end);
%% grafy kv�li n�hledu zkrelen�
figure(4)
hold off
plot(time,l_p,time,l_v,time,l_a,time,m2_c)
legend('l_p','l_v','l_a','m2_c')

figure(2)
hold off
plot(time,p_p,time,p_v,time,p_a,time,m1_c)
legend('p_p','p_v','p_a','m1_c')

figure(3)
cla reset
hold off
plot(time,angle_p,time,angle_v)
hold on
yyaxis right
plot(time,angle_a)
legend('angle_p','angle_v','angle_a')
% %% Pokus o pou�it� nejmen��ch �tverc� - nefunguje
% dvarphi(t) = diff(varphi(t),t);
% ddvarphi(t) = diff(dvarphi(t),t);
% 
% 
% % mnotnost kyvadla m_k "vynech�na" a v�echny ostant� konstany jsou "j� pod�leny", aby existovalo
% % jednozna�n� �e�en� a ne mno�ina �e�en�
% LeqrDissARed=subs(LeqrDissA,m_k,1);
% % dosazen� hodnot z�skan�ho z m��en�, v�etn� derivac�
% solvad  = subs(LeqrDissARed,... 
%     {varphi(t), dvarphi(t), ddvarphi(t),p(t),diff(p(t),t),diff(p(t),t,t),l(t),diff(l(t),t), diff(l(t),t,t),in1,in2},...
%     {angle_p,   angle_v, angle_a ,p_p,    p_v,            p_a,    l_p,    l_v,                l_a,      m1_c,m2_c});
% 
% % um�l� operace aby se rovnice rozd�lili na �leny podle jednotliv�ch
% % konstant a dali se zapsat pomoc� metordy nejmen��ch �tverc�
% vars = symvar(vpa(solvad,8))
% eqns = vpa(solvad,8)+ 0.00000000000000000000000001*(sum(vars(2:end))+1);
% separ = children(eqns);
% separr = vpa(subs(separ),8);
% 
% %kdy� jsou jednotliv� �leny rozd�leny jsou vymaz�ny symbolick� prom�nn�
% separr = subs(separr,{b_v,b_m,m_m,m_v,t_m1,t_m2,b_k},{1 1 1 1 1 1 1});
% separd = double(round(double(separr),15));
% 
% % samotn� nejmen�� �tverce a zp�tn� p�i�azen� prom�n�ch pro leh�� �ten�
% konstanty = separd(:,1:end-1)\(-separd(:,end))
% konstanySPromenymi = konstanty'.*[vars(2:end)];
% vpa(konstanySPromenymi',5)

%% Model, kter� bere hodnoty z jin�ho 
% mnotnost z�va�� "vynech�na" a v�echny ostant� konstany jsou j� pod�leny, aby existovalo
% jednozna�n� �e�en� a ne mno�ina �e�en�
syms dphi2dt2

Leqrc = subs(LeqrDissPhi,{m_k,diff(varphi(t),t,t)},{1,dphi2dt2})

dphi2dt2_sol = solve(Leqrc,dphi2dt2)

syms omega(t)

f2 = subs(dphi2dt2_sol,diff(varphi(t),t),omega(t))
f1 = omega(t)
f = [f1;f2]
time = time - min(time); % pro zjednodu�en� posunut za��tek �asu
syms t

%funkce ur�� parametry sinusovky, kter� je vstupn�m sign�lem
[Omega,Beta,Amp,Offset,Omega0]=FitExpSin(time,l_p+p_p,5); 
l(t) = Offset+Amp*sin(Omega*t+Omega0)
figure(15)
hold off; plot(time,l_p+p_p)
hold on;  plot(time,l(time))
[Omega,Beta,Amp,Offset,Omega0]=FitExpSin(time,p_p,6);
p(t) = Offset+Amp*sin(Omega*t+Omega0)
figure(16)
hold off; plot(time,p_p)
hold on;  plot(time,p(time))


b_k = 0.05
f = subs(f)

odefun = odeFunction(f,[varphi(t) omega(t)]);

% Setting the simulation parameters and calling an ODE solver
tspan = [min(time) max(time)];
initConditions = [angle_p(1); angle_v(1);];

[tim,x] = ode45(odefun, tspan, initConditions);

% Plotting the solutions
figure(1)
subplot(4,1,1)
hold off
plot(tim,l(tim))
hold on
plot(time,l_p)
ylabel('l(t) [m]')

subplot(4,1,2)
hold off
plot(tim,p(tim))
hold on
plot(time,p_p)
ylabel('p(t) [m]')

subplot(4,1,3)
hold off
plot(tim,x(:,1))
hold on
plot(time,angle_p)
ylabel('\phi(t) [rad]')
legend("fit","data")

subplot(4,1,4)
hold off
plot(tim,x(:,2))
hold on
plot(time,angle_v)
xlabel('t [s]')
ylabel('\omega(t) [rad/s]')


