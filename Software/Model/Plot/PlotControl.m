% load('rizeniSpatnaTrajektorie.mat')
% load('rizeniSpatnaTrajektorie2.mat')
close all
StatesK0 = StatesK7bad.signals.values(:,1:6)
timeK0 = StatesK7bad.time
StatesK1 = StatesK8bad.signals.values(:,1:6)
timeK1 = StatesK8bad.time
StatesK2 = StatesK6bad.signals.values(:,1:6)
timeK2 = StatesK6bad.time
%tsim, X
[XoutSim,YoutSim] = StatesToPos(X')
[XoutStar,YoutStar] = StatesToPos(x_star')
[XoutK0,YoutK0] = StatesToPos(StatesK0')
[XoutK1,YoutK1] = StatesToPos(StatesK1')
[XoutK2,YoutK2] = StatesToPos(StatesK2')

figure(56)
  
hold on
plot(XoutK0(1:end),-YoutK0(1:end),'-');     % trajektorie 
plot(XoutK1(1:end),-YoutK1(1:end),'-');     % trajektorie 
plot(XoutK2(1:end),-YoutK2(1:end),'-');     % trajektorie 
plot(XoutSim(1:end),-YoutSim(1:end),'-');     % trajektorie 
plot(XoutStar(1:end),-YoutStar(1:end),'-'); 
legend("PID","LQR - kone�n� horizont","LQR - nekon�en� horizont","simulace","C�lov� trajektorie",'location','best')
hold off;grid on;
axis equal;
axis([-0.25,0.55, -1.0,-0.3]);
% set(gca, 'XDir', 'reverse');
% set(gca, 'YDir', 'reverse');
xlabel("K_x [m]");ylabel("K_y [m]");

figure(57)
XoutStarInterp = @(t) interp1(t_star(1:end-2),XoutStar(1:end-2),t,'previous','extrap');
YoutStarInterp = @(t) interp1(t_star(1:end-2),YoutStar(1:end-2),t,'previous','extrap');
hold off
plot(timeK0',XoutK0'-XoutStarInterp(timeK0))
hold on; grid on
plot(timeK1',XoutK1'-XoutStarInterp(timeK1))
plot(timeK2',XoutK2'-XoutStarInterp(timeK2))
plot(tsim',XoutSim'-XoutStarInterp(tsim))
legend("PID","LQR - kone�n� horizont","LQR - nekon�en� horizont","Simulace",'location','best'); xlim([0 4]);xlabel("t [s]");ylabel("|\DeltaK_x| [m]");

figure(58)
hold off
plot(timeK0',YoutK0'-YoutStarInterp(timeK0))
hold on; grid on
plot(timeK1',YoutK1'-YoutStarInterp(timeK1))
plot(timeK2',YoutK2'-YoutStarInterp(timeK2))
plot(tsim',YoutSim'-YoutStarInterp(tsim))
legend("PID","LQR - kone�n� horizont","LQR - nekon�en� horizont","Simulace",'location','best'); xlim([0 4]);xlabel("t [s]");ylabel("|\DeltaK_y| [m]");

figure(59)
hold off
plot(timeK0',sqrt((XoutK0'-XoutStarInterp(timeK0)).^2+(YoutK0'-YoutStarInterp(timeK0)).^2))
hold on; grid on
plot(timeK1',sqrt((XoutK1'-XoutStarInterp(timeK1)).^2+(YoutK1'-YoutStarInterp(timeK1)).^2))
plot(timeK2',sqrt((XoutK2'-XoutStarInterp(timeK2)).^2+(YoutK2'-YoutStarInterp(timeK2)).^2))
plot(tsim',sqrt((XoutSim'-XoutStarInterp(tsim)).^2+(YoutSim'-YoutStarInterp(tsim)).^2))
legend("PID","LQR - kone�n� horizont","LQR - nekon�en� horizont","Simulace",'location','best'); xlim([0 4]);xlabel("t [s]");ylabel("|\DeltaK| [m]");


saveas(figure(56),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\RegTraject2','epsc')
saveas(figure(57),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\RegDX2','epsc')
saveas(figure(58),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\RegDY2','epsc')
saveas(figure(59),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\RegOdchylka2','epsc')