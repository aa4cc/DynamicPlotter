syms Kx Ky
Setpoints = [[0, 0.5];  [0.5, 0.6];  [0, 0.8];  [-0.2, 0.6]; [0, 0.5]]
% Setpoints = [[0, 0.5];  [0.5, 0.5];  [0.5, 0];  [-0.2, 0.6]; [0, 0.5]]

point = [Kx ;  Ky];

index = 1
Origin = Setpoints(index,:)
Goal = Setpoints(index+1,:)
Vector1 = Goal-Origin;
Vector1=Vector1/sqrt(Vector1*Vector1');
Vector1x = Vector1(1);
Vector1Y = Vector1(2);
Projektor1  = [-Vector1Y; Vector1x]*[-Vector1Y Vector1x]/(Vector1x^2+Vector1Y^2)
distance1 = sum((Projektor1*(point-Origin')).^2)


% index = 0
Origin = Setpoints(index+1,:)
Goal = Setpoints(index+2,:)
Vector2 = Goal-Origin;
Vector2=Vector2/sqrt(Vector2*Vector2');
Projektor2 = eye(2) - (Vector2'*Vector2/(Vector2*Vector2'))


distance2 = sum((Projektor2*(point-Origin')).^2)

[Kxs,Kys] = solve(distance1 == 0.03^2, distance2 == 0.03^2)
double(Kxs), double(Kys)
figure(89)
plot(double(Kxs),double(Kys))
%%

xin = [     0.0042,0.3952,0.007225,-0.1494,0.0042 ]
yin = [0.5314 ,  0.6096 ,0.7648,0.6082,0.5314 ]
xout = [-0.0042,    0.60,-0.007225,-0.2506,-0.0042 ]
yout = [ 0.4686 ,0.5904 ,.8352 ,0.5918,0.4686]

% figure(42)
% plot(xin,yin)
% hold on 
% plot(xout,yout)
%%
figure(86)
curve1 = yin;
curve2 = yout;
hold on;
x2 = [xin, xout];
inBetween = [curve1, fliplr(curve2)];
h =fill(x2, -inBetween, 'g');
set(h,'facealpha',.1)



r= 0.01
hold on
index = 1
th = 0:pi/50:2*pi;
x_circle1 = r * cos(th) +  Setpoints(index,1);
y_circle1 = r * sin(th) + Setpoints(index,2);
x_circle2 = r * cos(th) +  Setpoints(index+1,1);
y_circle2 = r * sin(th) + Setpoints(index+1,2);
x_circle3 = r * cos(th) +  Setpoints(index+2,1);
y_circle3= r * sin(th) + Setpoints(index+2,2);
x_circle4 = r * cos(th) +  Setpoints(index+3,1);
y_circle4 = r * sin(th) + Setpoints(index+3,2);
fill(x_circle1, -y_circle1,'r',x_circle2, -y_circle2, 'r',x_circle3, -y_circle3, 'r',x_circle4, -y_circle4, 'r')
legend('Euler simulace','Euler pl�nov�n�','Runge-Kutte simulace','Runge-Kutte pl�nov�n�','Hermit-Simpson simulace','Hermit-Simpson pl�nov�n�','Omezen� vzd�lenosti od p��mek','Omezen� vzd�lenosti od bod�','location','best')
xlabel('K_x [m]');ylabel('K_y [m]')
axis([-0.3,0.60,-1.1,-0.45]);
