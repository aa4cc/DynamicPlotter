load('rizeniSpatnaTrajektoriePosXVelXCur.mat')
close all
StatesK0 = StatesKbadCur.signals.values(:,1:6)
timeK0 = StatesKbadCur.time
StatesK1 = StatesKbadVel.signals.values(:,1:6)
timeK1 = StatesKbadVel.time
StatesK2 = StatesKbadPos.signals.values(:,1:6)
timeK2 = StatesKbadPos.time
%tsim, X
[XoutSim,YoutSim] = StatesToPos(X')
[XoutStar,YoutStar] = StatesToPos(x_star')
[XoutK0,YoutK0] = StatesToPos(StatesK0')
[XoutK1,YoutK1] = StatesToPos(StatesK1')
[XoutK2,YoutK2] = StatesToPos(StatesK2')

figure(56)
  
hold on
plot(XoutK0(1:end),YoutK0(1:end),'-');     % trajektorie 
plot(XoutK1(1:end),YoutK1(1:end),'-');     % trajektorie 
plot(XoutK2(1:end),YoutK2(1:end),'-');     % trajektorie 
plot(XoutStar(1:end),YoutStar(1:end),'-'); 
legend("Proudov� ��zen�","Rychlostn� ��zen�","Pozi�n� ��zen�","C�lov� trajektorie",'location','best')
hold off;grid on;
axis equal;
axis([-0.3,0.5, 0.3,1.0]);
set(gca, 'XDir', 'reverse');
set(gca, 'YDir', 'reverse');xlabel("x [m]");ylabel("y [m]");

figure(57)
XoutStarInterp = @(t) interp1(t_star(1:end-2),XoutStar(1:end-2),t,'previous','extrap');
YoutStarInterp = @(t) interp1(t_star(1:end-2),YoutStar(1:end-2),t,'previous','extrap');
hold off
plot(timeK0',XoutK0'-XoutStarInterp(timeK0))
hold on; grid on
plot(timeK1',XoutK1'-XoutStarInterp(timeK1))
plot(timeK2',XoutK2'-XoutStarInterp(timeK2))
legend("Proudov� ��zen�","Rychlostn� ��zen�","Pozi�n� ��zen�",'location','best')
xlim([0 5]);xlabel("t [s]");ylabel("chyba [m]");


figure(58)
hold off
plot(timeK0',YoutK0'-YoutStarInterp(timeK0))
hold on; grid on
plot(timeK1',YoutK1'-YoutStarInterp(timeK1))
plot(timeK2',YoutK2'-YoutStarInterp(timeK2))
legend("Proudov� ��zen�","Rychlostn� ��zen�","Pozi�n� ��zen�",'location','best');
xlim([0 5]);xlabel("t [s]");ylabel("chyba [m]");

figure(59)
hold off
plot(timeK0',sqrt((XoutK0'-XoutStarInterp(timeK0)).^2+(YoutK0'-YoutStarInterp(timeK0)).^2))
hold on; grid on
plot(timeK1',sqrt((XoutK1'-XoutStarInterp(timeK1)).^2+(YoutK1'-YoutStarInterp(timeK1)).^2))
plot(timeK2',sqrt((XoutK2'-XoutStarInterp(timeK2)).^2+(YoutK2'-YoutStarInterp(timeK2)).^2))
legend("Proudov� ��zen�","Rychlostn� ��zen�","Pozi�n� ��zen�",'location','best');
xlim([0 4]);xlabel("t [s]");ylabel("|\DeltaK| [m]");


saveas(figure(56),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\PosVelTraject','epsc')
% saveas(figure(57),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\RegDX','epsc')
% saveas(figure(58),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\RegDY','epsc')
saveas(figure(59),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\PosVelOdchylka','epsc')