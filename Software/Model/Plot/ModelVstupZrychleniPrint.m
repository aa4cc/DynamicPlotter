clear all
clc

% %Nacten� dat a p�e�k�lov�n� z hodnot enkoder� na re�ln� hondnoty - bezprobl�m� funguje
load('data09032020-2.mat')

dataset=kmity1;
T0 = 10;    %zacatecni �as
T1 = 20;    %konecny �as
Fsampling = 1/(dataset.time(2)-dataset.time(1));

angle_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,9);
angle_p = (angle_p-mean(angle_p))*2*pi/8192; % �hel kmit�n� kyvadla

% poloha kyvadla
m1_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,2)/8192*0.040;
m2_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,4)/8192*0.040*2.5;


m1_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,7);
m2_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,8);
time = dataset.time(Fsampling*T0:Fsampling*T1);

p_p = -m1_p; % pozice voz�ku
l_p = +m1_p-m2_p+(37418/8192*0.040*2.5); % d�lka z�v�su
% sni�ov�n� vzorkovac� frekvence - pokus o sn�en� �umu p�i derivac�ch
downrate = 3;
Fsampling = Fsampling/downrate; 
angle_p=subsamplingAverage(angle_p,downrate);
p_p=subsamplingAverage(p_p,downrate);
l_p=subsamplingAverage(l_p,downrate);
m1_c=subsamplingAverage(m1_c,downrate);
m2_c=subsamplingAverage(m2_c,downrate);
time=subsamplingAverage(time,downrate);

% Derivace a filtrov�n� a osek�n� okraj� m��en� kv�li filtr�m a derivac�m - �ekl bych, �e funguje dob�e
sampleDelay = 30;
angle_v = diff(angle_p)*Fsampling;
angle_a = diff(angle_v)*Fsampling;
l_v =  diff(l_p)*Fsampling;
l_a =  diff(l_v)*Fsampling;
p_v = diff(p_p)*Fsampling;
p_a = diff(p_v)*Fsampling;



angle_a = angle_a(sampleDelay:end);
angle_v = angle_v(sampleDelay+1:end);
angle_p = angle_p(sampleDelay+2:end);


p_p = p_p(sampleDelay+2:end);
p_v = p_v(sampleDelay+1:end);
p_a = p_a(sampleDelay:end);



l_p = l_p(sampleDelay+2:end);
l_v = l_v(sampleDelay+1:end);
l_a = l_a(sampleDelay:end);

m1_c = m1_c(sampleDelay+2:end);
m2_c = m2_c(sampleDelay+2:end);
time = time(sampleDelay+2:end);







%% Symbolic variables and functions
% m_k - hmotnost kyvadla 
% m_v - hmotnost voz�ku, m_m - setrva�nost motoru, 
% b_m - tlumen� motoru, b_v - tlumen� vz�tka,  b_k - tlumen� kyvadla 
% t_m1 - p�evodn� konstanta pro motor 1, t_m2 - p�evodn� konstanta pro motor 2 
syms m_k m_v m_m b_m b_v b_k t_m1 t_m2 b_vs b_ms


% l - delka z�v�su
% varphi - �hel kyvadla
% p - poloha voz�tka
% in - vstupn� proudy jednotliv�ch motor�
syms l(t) varphi(t) p(t) in1(t) in2(t)


%% Coordinates of the pendulum

x(t) = (l(t)-p(t))*sin(varphi(t))+p(t)
y(t) = (l(t)-p(t))*cos(varphi(t))

%% Computing the velocities

vx(t) = diff(x(t),t)
vy(t) = diff(y(t),t)
vp(t) = diff(p(t),t)
vl(t) = diff(l(t),t)


%% Lagrange equation
%       zavazi                        vozitko + jeho motor      motor lanka        
T = 1/2 *m_k* ((vx(t))^2 + vy(t)^2) + 1/2 * (m_v) * vp(t)^2 +  1/2 * m_m * vl(t)^2
V =  -9.81*m_k*y(t)
L = T-V

LeqrA = -functionalDerivative(L,[varphi l p])
LeqrPhi = -functionalDerivative(L,varphi)
disA = [-b_k*diff(varphi,t)*(l(t)-p(t))^2 ; -b_m*diff(l,t)+t_m1*in1 - b_ms*sigmoidd(diff(l,t)) ; -(b_v)*diff(p,t)+t_m2*in2- b_vs*sigmoidd(diff(p,t))]
disPhi = -b_k*diff(varphi,t)*(l(t)-p(t))^2 

LeqrDissA = LeqrA - disA
LeqrDissAEq = LeqrA == disA
LeqrDissPhi = LeqrPhi - disPhi
LeqrDissAEq = LeqrPhi == disPhi

%% Vyjadreni druhych derivaci za ucelem ziskani stavoveho modelu
syms ls Dls DDls phis Dphis DDphis ps Dps DDps inls inps
eqs = subs(LeqrDissPhi, ...
    {l(t), diff(l(t), t), diff(l(t), t, t), varphi(t), diff(varphi(t), t), diff(varphi(t), t, t), p(t), diff(p(t), t), diff(p(t), t, t), in1(t), in2(t)},...
    {ls, Dls, DDls, phis, Dphis, DDphis, ps, Dps, DDps, inls, inps});
eqs = subs(eqs,{ m_v, m_m, b_m, b_v, t_m1, t_m2, b_vs, b_ms},{0,0,0,0,0,0,0,0});
m_zavazi= 0.101;
eqs = subs(eqs,{m_k,b_k},{m_zavazi   0.025*m_zavazi})
eqs = subs(eqs,{DDps,DDls},{inps   inls})

%       [m_k;          b_k];)
ddphi = solve(eqs,  DDphis);

% state
x = [ls; Dls; phis; Dphis; ps; Dps];
% control input
u = [inls;  inps];
% physical params
prms = [m_k b_k];
% Dx = f(x, u)
dynPlotter_simple_statespace_f = [ x(2);  
                            inls;
                            x(4);
                            ddphi;
                            x(6);
                            inps;]

dynPlotter_simple_statespace_f = simplify(dynPlotter_simple_statespace_f);                        
                        
%matlabFunction(dynPlotter_simple_statespace_f, 'Vars',{x, u, prms}, 'File', 'simple_dynPlotter_ode');
matlabFunction(dynPlotter_simple_statespace_f, 'Vars',{x, u}, 'File', 'simple_dynPlotter_ode');

%% Test the model
endtime = 5
% snaha o vypo��t�n� v�ech veli�in
m_zavazi= 0.101;
% prms = [m_zavazi;   0.050*m_zavazi]
%       [m_k;          b_k];



% % Control inputs:
% % vstup nafitovan� sinusovka - numericky stabiln�j��
[Omega,Beta,Amp,Offset,Omega0]=FitExpSin(time,p_a,6);
a_cart = @(t) Offset+Amp*sin(Omega*t+Omega0)
%a_cart = @(t) 0
[Omega,Beta,Amp,Offset,Omega0]=FitExpSin(time,l_a+p_a,7)
a_line = @(t)  (Offset+Amp*sin(Omega*t+Omega0));
%a_line = @(t)  0;

a_cart = @(t) interp1(time,p_a,t);%,'spline'
a_line = @(t) interp1(time,l_a+p_a,t);


% x0 = [0.5;0;0;0;0;0];
%x0 = [0.5;0;0;0;0;0];
x0 = [l_p(1)+ p_p(1); l_v(1)+p_v(1);    angle_p(1);     angle_v(1);  p_p(1);   p_v(1)];
%x = [ls; Dls; phis; Dphis; ps; Dps];

% Simulation
options = odeset('RelTol',1e-5,'Stats','on');
%[t, X] = ode45(@(t, x) simple_dynPlotter_ode(x, [a_line ; a_cart], prms), [0; endtime], x0);
[tim, X] = ode45(@(t, x) simple_dynPlotter_ode(x,[a_cart(t); a_line(t)]), [0; endtime], x0);

% Interpolation
t2 = min(tim):1/25:max(tim);
X2 = interp1(tim,X,t2);


Xpen = X2(:,5)+(X2(:,1)-X2(:,5)).*sin(X2(:,3));
Ypen = (X2(:,1)-X2(:,5)).*cos(X2(:,3));

  % vizualizace
figure(1)
for cnt = 1:numel(t2)
    xtemp = X2(cnt,5);
    phi = X2(cnt,3);
    fill([-100 100 100 -100]*1e-3+xtemp,[0 0 100 100]*1e-3,[.5 .5 .5]); % vozik
    hold on;
    plot([-100, xtemp,Xpen(cnt)],[0,0,Ypen(cnt)],'o-');                         % zaves
    plot(Xpen(1:cnt),Ypen(1:cnt),'-');                              % trajektorie
    hold off;
    axis equal;
    axis([-2,2,-0.2,2.0]);
    set(gca, 'XDir', 'reverse');
    set(gca, 'YDir', 'reverse');
    pause(0.0001);
end

time = time - min(time);
% Plotting the solutions
figure(2)
subplot(4,2,1)
hold off
plot(t2,(X2(:,1)-X2(:,5)))
hold on; plot(time,l_p)
ylabel('l(t) [m]');xlim([0,endtime])

subplot(4,2,2)
hold off
plot(t2,(X2(:,2)-1*X2(:,6)))
hold on; plot(time,l_v)
ylabel('dl(t) [m/s]');xlim([0,endtime])


subplot(4,2,3)
hold off;plot(t2,X2(:,5))
hold on;plot(time,p_p)
ylabel('p(t) [m]');xlim([0,max(t2)])

subplot(4,2,4)
hold off;plot(t2,X2(:,6))
hold on;plot(time,p_v)
ylabel('dp(t) [m/s]');xlim([0,max(t2)])

subplot(4,2,5)
hold off; plot(t2,X2(:,3))
hold on; plot(time,angle_p)
ylabel('\phi(t) [rad]'); legend("fit","data");xlim([0,max(t2)])

subplot(4,2,6)
hold off;plot(t2,X2(:,4))
hold on;plot(time,angle_v)
xlabel('t [s]'); ylabel('\omega(t) [rad/s]');xlim([0,max(t2)])

subplot(4,2,7)
hold off;plot(t2,X2(:,1))
hold on;plot(time,l_p+p_p)
xlabel('t [s]'); ylabel('L(t)');xlim([0,max(t2)])

subplot(4,2,8)
hold off;plot(t2,X2(:,2))
hold on;plot(time,l_v+p_v)
xlabel('t [s]'); ylabel('DL(t)');xlim([0,max(t2)])

figure(67)
hold off; plot(t2,X2(:,3))
hold on; plot(time,angle_p)
ylabel('$\phi(t) [rad]$','interpreter','latex'); legend("Matematick� model","Re�ln� model");xlim([0,max(t2)]);grid on;xlabel("t [s]")
% saveas(figure(67),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\grafTestModelZrychleni','epsc')

%%
n = 35;
Tf = 5;
x0 = [0;0];
Setpoints = [[0, 0.5];[0.05, 0.525];  [0.1, 0.55];[0.05, 0.575];  [0, 0.6];[-0.05, 0.575];  [-0.1, 0.55]; [-0.05, 0.525];  [0, 0.5]]
Setpoints = [[0, 0.5];  [0.2, 0.6];  [0, 0.7];  [-0.2, 0.6]; [0, 0.5]];
NumSetpoints = length(Setpoints)
N = n*(NumSetpoints-1)
method = 'euler';
%method = 'rk4';
%method = 'collocation';

umin = -2.75;
umax = 2.75;
umin = -5;
umax = -umin;
% umin = -Inf;
% umax = Inf;

% Initialize the optimization problem
opti = casadi.Opti();

% define decision variables
X = opti.variable(6,N+1); % states
LO = X(1, :);
DLO = X(2, :);
phiO = X(3, :);
DphiO = X(4, :);
pO = X(5, :);
DpO = X(6, :);
xO = (LO-pO).*sin(phiO)+pO
yO = (LO-pO).*cos(phiO)

U = opti.variable(2,N); % controls
timepart = opti.variable(1,NumSetpoints-1)


% Objective function (minimize the control effort)
%opti.minimize(sum(timepart)+0.0005*sum(timepart).*sum(U));%
movecost = 0
for index=1:NumSetpoints-1
   movecost = movecost+timepart(:,index)* sum(sum(U(:,(index-1)*n+1:index*n).*U(:,(index-1)*n+1:index*n)))
end
opti.minimize(sum(timepart)+0.0005*movecost);
% Dynamic constraints
f = @(x,u) simple_dynPlotter_ode(x, u); % dx/dt = f(x,u)
dt = Tf/N; % control interval length

switch lower(method)
    case 'euler'
        % ---- Forward Euler --------
        for index=1:NumSetpoints-1  
            for index2=(index-1)*n+1:index*n % loop over control intervals
                x_next = X(:,index2) + timepart(:,index)*f(X(:,index2), U(:,index2));
                opti.subject_to(X(:,index2+1)==x_next); % close the gaps
            end
        end
        case 'rk4'
        % ---- RK4 --------
        for index=1:NumSetpoints-1 
            dt = timepart(:,index)
            for index2=(index-1)*n+1:index*n % loop over control intervals
                k1 = f(X(:,index2),         U(:,index2));
                k2 = f(X(:,index2)+dt/2*k1, U(:,index2));
                k3 = f(X(:,index2)+dt/2*k2, U(:,index2));
                k4 = f(X(:,index2)+dt*k3,   U(:,index2));
                x_next = X(:,index2) + dt/6*(k1+2*k2+2*k3+k4);
                opti.subject_to(X(:,index2+1)==x_next); % close the gaps
            end
        end
        case 'collocation' %broken
        % ---- Collocation --------
        for index=1:NumSetpoints-1 
            dt = timepart(:,index)
            for index2=(index-1)*n+1:index*n % loop over control intervals
                u1 = U(:,index2);
                if index2 ~=  N
                    u2 = U(:,index2+1);
                else
                    u2 = U(:,index2);
                end

                x1 = X(:,index2);
                x2 = X(:,index2+1);

                f1 = f(x1, u1);
                f2 = f(x2, u2);
                xc = (x1 + x2)/2 + dt*(f1 - f2)/8;
                fc = f(xc, (u1 + u2)/2);

                opti.subject_to(fc + 3*(x1-x2)/2/dt + (f1+f2)/4 == 0); % close the gaps        
            end
        end
    otherwise
        error('Unknown method was chosen!');
end

% Initial conditions
opti.subject_to(pO(1)==0);   % start at position 0 ...
opti.subject_to(DpO(1)==0); % ... from stand-still 
opti.subject_to(LO(1)==Setpoints(1,2));   % start at position 0 ...
opti.subject_to(DLO(1)==0); % ... from stand-still
opti.subject_to(phiO(1)==0);   % start at position 0 ...
opti.subject_to(DphiO(1)==0); % ... from stand-still

for index  = 2:NumSetpoints-1
    opti.subject_to(xO(n*(index-1))==Setpoints(index,1))
    opti.subject_to(yO(n*(index-1))==Setpoints(index,2))
end

% Final-time constraints

opti.subject_to(pO(end)==0);   % start at position 0 ...
opti.subject_to(DpO(end)==0); % ... from stand-still 
opti.subject_to(LO(end)==Setpoints(end,2));   % start at position 0 ...
opti.subject_to(DLO(end)==0); % ... from stand-still
opti.subject_to(phiO(end)==0);   % start at position 0 ...
opti.subject_to(DphiO(end)==0); % ... from stand-still


opti.subject_to(umin<=U<=umax);
opti.subject_to(0.001<=timepart<=Tf/n);
% Initialize decision variables
opti.set_initial(U, 0);
opti.set_initial(pO, 0);
opti.set_initial(DpO, 0);
opti.set_initial(LO, Setpoints(1,2));
opti.set_initial(DLO, 0);
opti.set_initial(phiO, 0);
opti.set_initial(DphiO, 0);

% Solve NLP
opti.solver('ipopt'); % use IPOPT solver
sol = opti.solve();

%%
x_nlp = sol.value(X)';
u_nlp = sol.value(U)';
dt = sol.value(timepart(:,1))';
Tset = [0];
t_nlp = (0:n)*dt
t_nlp2 = (0:n)*dt+dt/2
Tset= [Tset,max(t_nlp)];
lastdt = dt
for k = (2:(NumSetpoints-1))
    dt = sol.value(timepart(:,k))';
    t_nlp = [t_nlp, (1:n)*dt + max(t_nlp)];
    t_nlp2 = [t_nlp2, (1:n)*dt + max(t_nlp2)+dt/2-0.5*lastdt];
    Tset= [Tset,max(t_nlp)];
    lastdt = dt
end

LOO = x_nlp(:, 1);
DLOO = x_nlp(:, 2);
phiOO = x_nlp(:, 3);
DphiOO = x_nlp(:, 4);
pOO = x_nlp(:, 5);
DpOO = x_nlp(:, 6);

xOO = (LOO-pOO).*sin(phiOO)+pOO;
yOO = (LOO-pOO).*cos(phiOO);

% Extract the states and control from the decision variables




a_cart = @(t) interp1(t_nlp2(1:end-1),u_nlp(:,1),t,'spline');%,'spline'
a_line = @(t) interp1(t_nlp2(1:end-1),u_nlp(:,2),t,'spline');%,'spline'

endtime =max(t_nlp)
x0 = [LOO(1); DLOO(1);    phiOO(1);     DphiOO(1);  pOO(1);   DpOO(1)];

% Simulation
[tsim, X] = ode45(@(t, x) simple_dynPlotter_ode(x,[a_cart(t); a_line(t)]), [0; endtime], x0);
%%

% Interpolation
t2 = min(t_nlp):1/200:max(t_nlp);
X2 = interp1(tsim,X,t2);
X3 = interp1(t_nlp,x_nlp,t2);

Xpen = X2(:,5)+(X2(:,1)-X2(:,5)).*sin(X2(:,3));
Ypen = (X2(:,1)-X2(:,5)).*cos(X2(:,3));

Xpen2 = X3(:,5)+(X3(:,1)-X3(:,5)).*sin(X3(:,3));
Ypen2 = (X3(:,1)-X3(:,5)).*cos(X3(:,3));

  % vizualizace
figure(1)
for cnt = 1:numel(t2)
    xtemp = X2(cnt,5);
    %phi = X2(cnt,3);
    fill([-100 100 100 -100]*1e-3+xtemp,[0 0 100 100]*1e-3,[.5 .5 .5]); % vozik
    hold on;
    plot([-100, xtemp,Xpen(cnt)],[0,0,Ypen(cnt)],'o-');                         % zaves
    plot([-100, X3(cnt,5),Xpen2(cnt)],[0,0,Ypen2(cnt)],'o-');                         % zaves
    plot(Xpen(1:cnt),Ypen(1:cnt),'-');                              % trajektorie    
    plot(Xpen2(1:cnt),Ypen2(1:cnt),'-');                              % trajektorie
    hold off;
    axis equal;
    axis([-1,1,-0.2,1.0]);
    set(gca, 'XDir', 'reverse');
    set(gca, 'YDir', 'reverse');
    pause(0.0001);
end
%%
figure(2)
subplot(4,2,1)
hold off
plot(t_nlp2',LOO-pOO)
hold on; plot(t2,(X2(:,1)-X2(:,5)))
ylabel('l(t) [m]');xlim([0,endtime])

subplot(4,2,2)
hold off
plot(t2,(X2(:,2)-1*X2(:,6)))
hold on; plot(t_nlp2',DLOO-DpOO)
ylabel('dl(t) [m/s]');xlim([0,endtime])


subplot(4,2,3)
hold off;plot(t2,X2(:,5))
hold on;plot(t_nlp2',pOO)
ylabel('p(t) [m]');xlim([0,max(t2)])

subplot(4,2,4)
hold off;plot(t2,X2(:,6))
hold on;plot(t_nlp2',DpOO)
ylabel('dp(t) [m/s]');xlim([0,max(t2)])

subplot(4,2,5)
hold off; plot(t2,X2(:,3))
hold on; plot(t_nlp2',phiOO)
ylabel('\phi(t) [rad]'); legend("model","optimalizace");xlim([0,max(t2)])

subplot(4,2,6)
hold off;plot(t2,X2(:,4))
hold on;plot(t_nlp2',DphiOO)
xlabel('t [s]'); ylabel('\omega(t) [rad/s]');xlim([0,max(t2)])

subplot(4,2,7)
hold off;plot(t2,X2(:,1))
hold on;plot(t_nlp2',LOO)
xlabel('t [s]'); ylabel('L(t)');xlim([0,max(t2)])

subplot(4,2,8)
hold off;plot(t2,X2(:,2))
hold on;plot(t_nlp2',DLOO)
xlabel('t [s]'); ylabel('DL(t)');xlim([0,max(t2)])


figure(15)
hold off
stairs(t_nlp(1:end-1), u_nlp);
hold on
plot(t2, a_cart(t2));
plot(t2, a_line(t2));
legend("opti cart","opti line","inter line","interp cart")


%% for simulink model
accelerations.time = t2';
accelerations.signals.values = [a_cart(t2)',a_line(t2)'];
accelerations.signals.dimensions =2;

angles.time = t_nlp2';
angles.signals.values = [phiOO];
angles.signals.dimensions =1;

RopeLen.time = t_nlp2';
RopeLen.signals.values = [LOO];
RopeLen.signals.dimensions =1;

CartPos.time = t_nlp2';
CartPos.signals.values = [pOO];
CartPos.signals.dimensions =1;

GoalStates.time =  t_nlp2';
GoalStates.signals.values = [x_nlp];
GoalStates.signals.dimensions =6;

m_zavazi= 0.101;
t_m1 = 1.90; % lane (0.033/(0.04/2/pi)/2.5  = 2.07) dle prvniho mereni % druhe mereni 1.66
t_m2 = 4.8; % cart (0.033/(0.04/2/pi)  = 5.18) dle prvniho mereni % druhe mereni 4.15
prms = [m_zavazi;       0.8;      0.2; 0.050*m_zavazi;         2.5; 9.2500;    t_m1;   t_m2;    4.5000; 0.2721];
         [m_k;          m_v;      m_m ;           b_k;         b_m;   b_v;     t_m1;   t_m2;     b_vs ;   b_ms];

%%
Y = [(ls-ps).*sin(phis)+ps;-(ls-ps).*cos(phis)]
dynPlotter_simple_statespace_f = simplify(dynPlotter_simple_statespace_f); 
xlin = [0.5; 0; 0; 0; 0; 0] 
jacA  = jacobian(dynPlotter_simple_statespace_f,x)
A = subs(jacA,x,xlin)
A = double(subs(A,u,[0;0]))
jacB  = jacobian(dynPlotter_simple_statespace_f,u)
B = double(subs(jacB,x,xlin))

jacC  = jacobian(Y,x)
C = double(subs(jacC,x,xlin))
D = zeros(2,2)


Q = [1 0 0 0 0 0;
    0 1 0 0 0 0;
    0 0 1 0 0 0;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1]
%Q  = C'*C
R = [0.1 0 ;
    0 0.1]
Co = ctrb(A,B); %CONTROLEABILITY MATRIX
K =lqr(A,B,Q,R)

sys = ss((A-B*K),B,C,D)
symtime = 0:0.005:30;
xdiff = [0.1,0.1,0.1,0.1,0.1,0.1]
[y,t,statesInitial] = initial(sys, xdiff, symtime);
figure(123)
plot(t,statesInitial)
legend("L","DL","phi","dphi","p","dp")



