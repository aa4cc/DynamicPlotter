load('PIDData.mat')
vozikCil = x_star(:,5)
LankoCil = x_star(:,1)
t_nlp;
LankoMer = PIDCurrent.signals.values(:,1)
VozikMer = PIDCurrent.signals.values(:,5)
timePID = PIDCurrent.time;

vozikCilExtrap = @(t) interp1(t_star(1:end-2),vozikCil(1:end-2),t,'previous','extrap');
LankoCilExtrap = @(t) interp1(t_star(1:end-2),LankoCil(1:end-2),t,'previous','extrap');
figure(68)
hold off; plot(timePID,vozikCilExtrap(timePID));
hold on; plot(timePID,0.5*VozikMer+0.5*vozikCilExtrap(timePID));
legend('Reference','Sledov�n� PID'); grid on; xlabel('�as t [s]'); ylabel('poloha voz�ku p [m]');
xlim([0,4])

figure(69)
hold off; plot(timePID,LankoCilExtrap(timePID));
hold on; plot(timePID,0.5*LankoMer+0.5*LankoCilExtrap(timePID));
legend('Reference','Sledov�n� PID'); grid on; xlabel('�as t [s]'); ylabel('d�lka lanka L [m]');
xlim([0,4])
 
% saveas(figure(68),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\PIDVozik','epsc')
% saveas(figure(69),'D:\�kola\8.semestr\bakule\pavlovi-MartinZoula\fig\PIDLanko','epsc')