function visu( t, th, l )

figure(25)
clf

ax = axes();
h_title = title(sprintf('Time: %2.2f', 0));

alph = linspace(0, 2*pi, 20);
circ_x = cos(alph)';
circ_y = sin(alph)';

hold on

% String
h_string = plot(ax, [0 0], [0 l]);

% Ball
ball_r = 0.1;
ball_color = [0 0 1];
h_ball = fill(ax, ball_r*circ_x, ball_r*circ_y, ball_color);

hold off

set(gca,'YDir','Reverse');
axis(1.2*l*[-1 1 -1 1])
axis equal
xticks([])
yticks([])
box on


for i = 1:numel(t)
    tic
    
%     fprintf('Time %2.2f/%2.2f\n', t(i), t(end));
        
    set(h_string, 'XData', [0 l*sin(th(i))], 'YData', [0 l*cos(th(i))]);
    set(h_ball, 'XData', ball_r*circ_x + l*sin(th(i)), 'YData', ball_r*circ_y + l*cos(th(i)));
    set(h_title, 'String', sprintf('Time: %2.2f s', t(i)));
    
    t_elapsed = toc;
    if i < numel(t)
        dt = (t(i+1)-t(i));
        if (dt - t_elapsed) > 0
            pause((dt - t_elapsed));
        end
    end
end