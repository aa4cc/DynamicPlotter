%% 
clear all; clc; close all;
%% % skript na odvozeni rovnic matematického modelu
OdvozeniMatModel;
%%
NacteniDat;


%% Test the model
endtime = 4

NacteniParametruModelu;


% % Control inputs:
% % vstup nafitovaná sinusovka - numericky stabilnější
[Omega,Beta,Amp,Offset,Omega0]=FitExpSin(time,m1_c,6);
F_cart = @(t) Offset+Amp*sin(Omega*t+Omega0)
[Omega,Beta,Amp,Offset,Omega0]=FitExpSin(time,m2_c,7);
F_L = @(t)  (Offset+Amp*sin(Omega*t+Omega0));
% 

% Použít data jako vstup
% F_cart = @(t) interp1(time,m1_c,t);
% F_L = @(t) interp1(time,m2_c,t);%,'spline'
% % 



x0 = [l_p(1)+ p_p(1); l_v(1)+p_v(1);    angle_p(1);     angle_v(1);  p_p(1);   p_v(1)];
%x = [Ls; DLs; phis; Dphis; ps; Dps];

% Simulation
options = odeset('RelTol',1e-5,'Stats','on');
[tsim, X] = ode45(@(t, x) dynPlotter_ode(x, [F_cart(t); F_L(t)], prms), [0; endtime], x0,options);

% Interpolation
t2 = min(tsim):1/25:max(tsim);
X2 = interp1(tsim,X,t2);


Xpen = X2(:,5)+(X2(:,1)-X2(:,5)).*sin(X2(:,3));
Ypen = (X2(:,1)-X2(:,5)).*cos(X2(:,3));

  % vizualizace
% figure(1)
% for cnt = 1:numel(t2)
%     x = X2(cnt,5);
%     phi = X2(cnt,3);
%     fill([-100 100 100 -100]*1e-3+x,[0 0 100 100]*1e-3,[.5 .5 .5]); % vozik
%     hold on;
%     plot([-100, x,Xpen(cnt)],[0,0,Ypen(cnt)],'o-');                         % zaves
%     plot(Xpen(1:cnt),Ypen(1:cnt),'-');                              % trajektorie
%     hold off;
%     axis equal;
%     axis([-2,2,-0.2,2.0]);
%     set(gca, 'XDir', 'reverse');
%     set(gca, 'YDir', 'reverse');
%     pause(0.0001);
% end

time = time - min(time);
% Plotting the solutions
figure(2)
legend("fit","data",'location','bestoutside')
subplot(4,2,1)
hold off
plot(t2,(X2(:,1)-X2(:,5)))
hold on; plot(time,l_p)
ylabel('l(t) [m]');xlim([0,endtime])

subplot(4,2,2)
hold off
plot(t2,(X2(:,2)-1*X2(:,6)))
hold on; plot(time,l_v)
ylabel('dl(t) [m/s]');xlim([0,endtime])


subplot(4,2,3)
hold off;plot(t2,X2(:,5))
hold on;plot(time,p_p)
ylabel('p(t) [m]');xlim([0,max(t2)])

subplot(4,2,4)
hold off;plot(t2,X2(:,6))
hold on;plot(time,p_v)
ylabel('dp(t) [m/s]');xlim([0,max(t2)])

subplot(4,2,5)
hold off; plot(t2,X2(:,3))
hold on; plot(time,angle_p)
ylabel('\phi(t) [rad]'); xlim([0,max(t2)])

subplot(4,2,6)
hold off;plot(t2,X2(:,4))
hold on;plot(time,angle_v)
xlabel('t [s]'); ylabel('\omega(t) [rad/s]');xlim([0,max(t2)])

subplot(4,2,7)
hold off;plot(t2,X2(:,1))
hold on;plot(time,l_p+p_p)
xlabel('t [s]'); ylabel('L(t)');xlim([0,max(t2)])

subplot(4,2,8)
hold off;plot(t2,X2(:,2))
hold on;plot(time,l_v+p_v)
xlabel('t [s]'); ylabel('DL(t)');xlim([0,max(t2)])
legend("data","fit",'location','best')
% hL = subplot(5,2,9);
% poshL = get(hL,'position');     % Getting its position
% 
% lgd = legend(hL,'RandomPlot1','RandomPlot2','RandomPlot3','RandomPlot4');
% set(lgd,'position',poshL);      % Adjusting legend's position
% axis(hL,'off');                 % Turning its axis off

figure(567)
hold off;plot(t2,X2(:,1))
hold on;plot(time,l_p+p_p)
xlabel('$t [s]$','interpreter','latex'); ylabel('$L(t) [m]$','interpreter','latex');xlim([0,max(t2)])
legend("Matematický model","Naměřená data");xlim([0,max(t2)]);grid on;
figure(568)
hold off;plot(t2,X2(:,5))
hold on;plot(time,p_p)
ylabel('$p(t) [m]$','interpreter','latex');xlim([0,max(t2)]); xlabel('$t [s]$','interpreter','latex');
legend("Matematický model","Naměřená data");grid on;


%% for simulink model
curents.time = time;
curents.signals.values = [m2_c,m1_c];
curents.signals.dimensions =2;

curent_mP_c.time = time;
curent_mP_c.signals.values = [m2_c];
curent_mP_c.signals.dimensions =1;

curent_mL_c.time = time;
curent_mL_c.signals.values = [m1_c];
curent_mL_c.signals.dimensions =1;

angles.time = time;
angles.signals.values = [angle_p];
angles.signals.dimensions =1;

RopeLen.time = time;
RopeLen.signals.values = [l_p+p_p];
RopeLen.signals.dimensions =1;
CartPos.time = time;
CartPos.signals.values = [p_p];
CartPos.signals.dimensions =1;


