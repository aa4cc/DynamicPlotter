%Nacten� dat a p�e�k�lov�n� z hodnot enkoder� na re�ln� hondnoty - bezprobl�m� funguje
load('data09032020-2.mat')

dataset=kmity2;
T0 = 10;    %zacatecni �as
T1 = 20;    %konecny �as
Fsampling = 1/(dataset.time(2)-dataset.time(1));

angle_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,9);
angle_p = (angle_p-mean(angle_p))*2*pi/8192; % �hel kmit�n� kyvadla

% poloha kyvadla
m1_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,2)/8192*0.040;
m2_p = dataset.signals.values(Fsampling*T0:Fsampling*T1,4)/8192*0.040*2.5;


m1_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,7);
m2_c = dataset.signals.values(Fsampling*T0:Fsampling*T1,8);
time = dataset.time(Fsampling*T0:Fsampling*T1);

p_p = -m1_p; % pozice voz�ku
l_p = +m1_p-m2_p+(37418/8192*0.040*2.5); % d�lka z�v�su
% sni�ov�n� vzorkovac� frekvence - pokus o sn�en� �umu p�i derivac�ch
downrate = 5;
Fsampling = Fsampling/downrate; 
angle_p=subsamplingAverage(angle_p,downrate);
p_p=subsamplingAverage(p_p,downrate);
l_p=subsamplingAverage(l_p,downrate);
m1_c=subsamplingAverage(m1_c,downrate);
m2_c=subsamplingAverage(m2_c,downrate);
time=subsamplingAverage(time,downrate);

% Derivace a filtrov�n� a osek�n� okraj� m��en� kv�li filtr�m a derivac�m - �ekl bych, �e funguje dob�e
sampleDelay = 30;
angle_v = diff(angle_p)*Fsampling;
angle_a = diff(angle_v)*Fsampling;
l_v =  diff(l_p)*Fsampling;
l_a =  diff(l_v)*Fsampling;
p_v = diff(p_p)*Fsampling;
p_a = diff(p_v)*Fsampling;



angle_p = angle_p(sampleDelay+2:end);
angle_v = angle_v(sampleDelay+1:end);
angle_a = angle_a(sampleDelay:end);


p_p = p_p(sampleDelay+2:end);
p_v = p_v(sampleDelay+1:end);
p_a = p_a(sampleDelay:end);

l_p = l_p(sampleDelay+2:end);
l_v = l_v(sampleDelay+1:end);
l_a = l_a(sampleDelay:end);

m1_c = m1_c(sampleDelay+2:end);
m2_c = m2_c(sampleDelay+2:end);
time = time(sampleDelay+2:end);


