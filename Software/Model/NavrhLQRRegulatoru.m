%clear all;clc;close all
PlanovaniTrajektorie;
%%

Ts = 1/250;
t_star =   min(t_states):Ts:max(t_states);
% t2 = min(t_nlp):1/250:max(t_nlp);
% X2 = interp1(tsim,X,t2);
% X3 = interp1(t_nlp,x_nlp,t2);
x_star = interp1(t_states,x_nlp,t_star);
u_star = interp1(t_UInterp(1:end-1),u_nlp(:,:),t_star,'pchip');

%%
%p�id�n �as na konec trajektorie, aby lqr regul�tor byl navr�en s
%p�edpokladem zastaven� v koncov�m bod� trajektorie
TimeAdded = 1;
adding = TimeAdded/Ts;
x_adding = repmat(x_star(1,:),adding,1);
x_star  = [x_star;x_adding];
t_adding = repmat(t_star(:,end),1,adding);
t_star  = [t_star,t_adding];
u_adding = repmat([0,0],adding,1);
u_star  = [u_star;u_adding];



% Weight matrices
QBase = eye(6);
% Q  = 0.5*Q +  C'*C
R = 0.05*eye(2);
%x statex,u inputs
KyvPos = [(Ls-ps).*sin(phis)+ps;-(Ls-ps).*cos(phis)];
KyvVel = [Dps + sin(phis)*(DLs - Dps) + cos(phis)*(Ls - ps)*Dphis;...
 cos(phis)*(DLs - Dps) - sin(phis)*(Ls - ps)*Dphis];
dynPlotter_simple_statespace_f = simplify(dynPlotter_simple_statespace_f); 

jacA  = jacobian(dynPlotter_simple_statespace_f,x);
jacB  = jacobian(dynPlotter_simple_statespace_f,u);
jacKyvPos  = jacobian(KyvPos,x);
jacKyvVel  = jacobian(KyvVel,x);


% Linearization around the optimal trajectory
tic;
A = zeros(6, 6, numel(t_star));
B = zeros(6, 2, numel(t_star));
AD = zeros(6, 6, numel(t_star));
BD = zeros(6, 2, numel(t_star));
CPenPos = zeros(2, 6, numel(t_star));
CPenVel = zeros(2, 6, numel(t_star));

for i=1:numel(t_star)
    xlin = x_star(i,:);
    ulin = u_star(i,:);
    A(:,:,i) = double(subs(jacA,[x;u],[xlin';ulin']));
    B(:,:,i) = double(subs(jacB,x,xlin'));
    CPenPos(:,:,i) = double(subs(jacKyvPos,x,xlin'));
    CPenVel(:,:,i) = double(subs(jacKyvVel,x,xlin'));
    Dk_c = zeros(2,2);
    
%     AD(:,:,i) = eye(6) + Ts*Ak_c;
%     BD(:,:,i) = Ts*Bk_c;
    
    sys = ss( A(:,:,i),B(:,:,i),CPenPos(:,:,i),Dk_c);
    sysD = c2d(sys,Ts,'zoh');
    AD(:,:,i) = sysD.A;
    BD(:,:,i) = sysD.B;
end


% Design a LQR stabilizying the trajectory
% Navr�eny dva regul�tory, jeden zahrnuje prom�nn� stav syst�mu a druh�
% po��t� s konstatn�mi stavy syst�mu
S = zeros(6, 6, numel(t_star));
K_prom = zeros(2, 6, numel(t_star));
K_stacion = zeros(2, 6, numel(t_star));


S(:,:, end) = QBase;
K_prom(:,:, end) = R\B(:,:, end)'*S(:,:, end);


%assuming the system is time invariant and linear
C_ack = CPenPos(:,:,end);
CV_ack = CPenVel(:,:,end);
QK  = 0.5*QBase +  2*C_ack'*C_ack +  CV_ack'*CV_ack;
K_stacion(:,:,end)= lqrd(A(:,:,end),B(:,:,end),QK,R,Ts);
K_stacion(:,:,:) = reshape(repmat( K_stacion(:,:,end),1,numel(t_star)),2,6,numel(t_star));


%assuming the system is linear time variant 
% Iterate the Difference Riccati Equation. 
for i = (numel(t_star) - 1):-1:1   
    C_ack = CPenPos(:,:,i);
    CV_ack = CPenVel(:,:,i);
    QK  = 0.5*QBase +  2*C_ack'*C_ack +  CV_ack'*CV_ack;
    K_prom(:,:,i) = (BD(:,:,i)'*S(:,:,i+1)*BD(:,:,i) + R)\(BD(:,:,i)'*S(:,:,i+1)*AD(:,:,i));
    S(:,:,i) = AD(:,:,i)'*S(:,:,i+1)*(AD(:,:,i) - BD(:,:,i)*K_prom(:,:,i)) + QK;
  end

toc;
%odebr�n� kone�n�ho �asu, kdy syst�m je ji� stacion�rn�
K_prom  = K_prom(:,:,1:end-adding+2) ;
K_stacion  = K_stacion(:,:,1:end-adding+2) ;
x_star  = x_star(1:end-adding+2,:);
u_star  = u_star(1:end-adding+2,:);
t_star  = t_star(:,1:end-adding+2);


K_prom = squeeze(K_prom);
K_stacion = squeeze(K_stacion);
K_prom = permute(K_prom,[2 1 3]);
K_stacion = permute(K_stacion,[2 1 3]);

%% p�evod na timeseries pro simulink
x0 = x_star(1,:);
Kprom_TS = timeseries(K_prom, t_star');
Kstat_TS = timeseries(K_stacion, t_star');
x_star_TS = timeseries(x_star, t_star');
u_star_TS = timeseries(u_star, t_star');




