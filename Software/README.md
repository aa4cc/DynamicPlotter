Popis software části projektu

Složka Odrive
obsahuje model pro simulink pro komunikaci simulinku s BeagleBone, zároveň vyčítá z vestavěného čítače kvadratického enkodéru (eQEP)


Složka Model
obsahuje všechny soubory použité při identifikaci modelu, návrhu trajektorie a regulátorů

Kalibrace modelu před použitím
Před zapojením připojením Odrive k napájení je nutné umístit vozík na střed osy. Na tomto místě bude umístěna nulová pozice vozíku. Délka závěsu je vypočítána z frekvence pokusných kmitů. Nejprve je potřeba spustit simulaci "DynamicPlotterCalib" a data vyhodnotit skriptem "OdriveKalibrace"




Casadi - https://web.casadi.org/get/
Pro výpočet trajektorie byl použit nástroj Casadi. Přesněji byla použita verze 3.4.4. ale jiné verze by měli fungovat bezproblémů. 
Tento nástroj není přiložen v Gitu, a proto je ho nutné znovu stáhnout. 