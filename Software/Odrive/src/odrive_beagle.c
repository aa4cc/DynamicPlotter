#include <stdio.h>
#include <string.h>
#include <errno.h>


#include <time.h>
#include <fcntl.h> 
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

 


#include "odrive_beagle.h"

#define timeout 10


int fd;
int f;
float EncoderOffset[2];
int error[2];
int timeoutTime[2];
float pos[2],vel[2],cur[2];

struct termios uartTermios,oldDescriptor;

float encoder(){
    int32_t position;
    FILE *fp = fopen("/sys/devices/ocp.3/48304000.epwmss/48304180.eqep/position", "r");
    // Check that we opened the file correctly
    if(fp == NULL)
    {
        // Error, break out
        fclose(fp);
        return 0;
    }

    // Write the desired value to the file
    fscanf(fp, "%d", &position);
    
    // Commit changes
    fclose(fp);
    return (float)(position);
    }
void odriveSetupDual(float vel_limit,float current_limit){
    char message[64];
    FILE *fp = (FILE *) f;
    int i = 0;
    int output=0; 
    int calibTime = 0; 
    int axis = 0;
    for(axis = 0;axis<2;axis++){
        i = sprintf (message,"r axis%i.current_state \r\n",axis);
        write(fd,message,i);
        usleep(5000);
        fgets(message, 60, fp); 
        sscanf(message,"%i",&output);      
        if ((output) != 8){
            i = sprintf (message,"w axis%i.requested_state 3 \r\n",axis); //w axis0.requested_state 3
            write(fd,message,i);  usleep(5000);
            calibTime = 15;
            } 
        }
   
    sleep(calibTime);
    
    for(axis = 0;axis<2;axis++){
        i = sprintf (message,"w axis%i.requested_state 8 \r\n",axis);
        write(fd,message,i);
        usleep(5000);
        i = sprintf (message,"w axis%i.motor.config.current_lim %i \r\n",axis,(int)current_limit);
        write(fd,message,i);    
        usleep(5000);
        i = sprintf (message,"w axis%i.controller.config.vel_limit %i \r\n",axis,(int) vel_limit);
        write(fd,message,i);
        usleep(5000);
    }
    return;
}
void digitalIOSetup(){
    
    fd =  open("/dev/ttyO1", O_RDWR | O_NOCTTY);
	//fd = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_SYNC);
    //int speed = B921600;
    int speed =  B460800;
    //int speed = B9600;
    //int speed = B115200;
    //int speed = B250000;
	/*baudrate 115200, 8 bits, no parity, 1 stop bit */
   // bzero(&uartTermios,sizeof(uartTermios));
    
    uartTermios.c_cflag =  CS8 | CLOCAL | CREAD ;
    uartTermios.c_iflag = IGNPAR | IGNCR;
    uartTermios.c_oflag = 0;
    uartTermios.c_lflag = 0;

    uartTermios.c_cc[VTIME] = 0.1;
    uartTermios.c_cc[VMIN]  = 0;
    cfsetspeed(&uartTermios,speed);
    tcflush(fd,TCIFLUSH);
    tcsetattr(fd,TCSANOW,&uartTermios);
    
/*    uartTermios.c_cflag = speed | CS8 | CLOCAL | CREAD;
    uartTermios.c_iflag = IGNPAR | ICRNL | IGNCR;
    uartTermios.c_oflag = 0;
    uartTermios.c_lflag = 0;

    uartTermios.c_cc[VTIME] = 0;
    uartTermios.c_cc[VMIN]  = 0;
    tcflush(fd,TCIFLUSH);
    tcsetattr(fd,TCSANOW,&uartTermios);
*/    
    

     f = (int) fdopen(fd, "r+");
	return;
}

void odriveSetupSingle(int axis,float vel_limit,float current_limit){
    char message[64];
    int i = 0;  
    FILE *fp = (FILE *) f;
    fgets(message, 60, fp); 
    i = sprintf (message,"r axis%i.current_state \r\n",axis);
    write(fd,message,i);
    usleep(5000);
    int output=0; 
    fgets(message, 60, fp); 
    sscanf(message,"%i",&output);      
    if ((output) != 8){
        i = sprintf (message,"w axis%i.requested_state 3 \r\n",axis); //w axis0.requested_state 3
        write(fd,message,i);
        sleep(15);
        i = sprintf (message,"w axis%i.requested_state 8 \r\n",axis);
        write(fd,message,i);
        usleep(5000);      }
    i = sprintf (message,"w axis%i.motor.config.current_lim %i \r\n",axis,(int)current_limit);
    write(fd,message,i);    
    usleep(5000);
    i = sprintf (message,"w axis%i.controller.config.vel_limit %i \r\n",axis,(int) vel_limit);
    write(fd,message,i);
   
}

int odriveReadFloat(float* data,int expected){
    char message[64];
    FILE *fp = (FILE *) f;
    int i = 0;
    unsigned int start = 0;
    
    fgets(message, 60, fp);     
    if (sscanf(message,"%f",data)!=1){
        return -1-i;
    }
    return start;
}


float readAndDrive(int axis, char type, float setpoint) {   
   FILE *fp = (FILE *) f;
   char message[64];
   int i = 0;
   //fgets(message, 60, fp);
   i = sprintf (message,"f %d \r\n", axis);
   write(fd,message,i);
   timeoutTime[axis] = 0;
   error[axis] =0;
   if (type=='p'){
        //i = sprintf (message, "%f %f %f", karel, 2*karel, 0.5*karel);
        i = sprintf (message,"p %d %f 0 0 \r\n", axis,setpoint);
        write(fd,message,i);
   }
   else if (type=='v'){
       
        i = sprintf (message,"v %d %f 0 \r\n", axis,setpoint);
        write(fd,message,i);
   }
    else if (type=='c'){
        
        i = sprintf (message,"c %d %f \r\n", axis,setpoint);
        write(fd,message,i);   }
    //request feedback
    usleep(1500);
    fgets(message, 60, fp);
    float posi,velo,curr;
    if (sscanf(message,"%f %f %f",&posi,&velo,&curr)==3){
        pos[axis] = posi;
        vel[axis] = velo;
        cur[axis] = curr;
        return posi;
        }
    pos[axis] = 0;
    vel[axis] = 0;
    cur[axis] = 0;
    return 0;
    
}
void DualReadAndDrive(char type, float PosSetpoint0, float PosSetpoint1,float VelSetpoint0, float VelSetpoint1,float CurSetpoint0, float CurSetpoint1) {
   tcflush (fd, TCIOFLUSH) ;
   char message[64];
   int i = 0;
   int axis = 0;
   for (axis =0;axis<2;axis++){
       i = sprintf (message,"f %d\r\n", axis);
       write(fd,message,i);
       timeoutTime[axis] = 0;
       error[axis] =0;
       }
   
   float PosSetpoints[2];   PosSetpoints[0]  = PosSetpoint0;   PosSetpoints[1]  = PosSetpoint1;
   float VelSetpoints[2];   VelSetpoints[0]  = VelSetpoint0;   VelSetpoints[1]  = VelSetpoint1;
   float CurSetpoints[2];   CurSetpoints[0]  = CurSetpoint0;   CurSetpoints[1]  = CurSetpoint1;
   for (axis =0;axis<2;axis++){
       if (type=='p'){
            //i = sprintf (message, "%f %f %f", karel, 2*karel, 0.5*karel);
            i = sprintf (message,"p %d %.1f %.1f %.2f\r\n", axis, PosSetpoints[axis], VelSetpoints[axis], CurSetpoints[axis]);
            write(fd,message,i);
       }
       else if (type=='v'){

            i = sprintf (message,"v %d %.1f %.2f\r\n", axis, VelSetpoints[axis], CurSetpoints[axis]);
            write(fd,message,i);
       }
        else if (type=='c'){
            i = sprintf (message,"c %d %.3f\r\n", axis,CurSetpoints[axis]);
            write(fd,message,i);}
    }
    usleep(2000);
    FILE *fp = (FILE *) f;
    float posi,velo,curr;
    for (axis =0;axis<2;axis++){
        fgets(message, 60, fp);
        if (sscanf(message,"%f %f %f",&posi,&velo,&curr)==3){
            pos[axis] = posi;
            vel[axis] = velo;
            cur[axis] = curr;
            }
        else{
            pos[axis] = 0;
            vel[axis] = 0;
            cur[axis] = 0;
            }  
    }
    i = sprintf (message,"\n");
    write(fd,message,i);
}
float readVoltage(){ 
    //serialPrintf(fd,"r vbus_voltage\n");
    float output;
    int time = odriveReadFloat(&output,10);
    if (time <= 0){
        return -2.22;        
    }
    else {
        return output;
    }
}
float readPosition(int axis){
    return pos[axis];
}
float readVelocity(int axis){
    return vel[axis];
}
float readCurrent(int axis){
    return cur[axis];
}
    



