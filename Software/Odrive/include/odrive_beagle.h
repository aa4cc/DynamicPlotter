#ifndef _ODRIVE_BEAGLE_H_
#define _ODRIVE_BEAGLE_H_
#include "rtwtypes.h"

void digitalIOSetup();
void DualReadAndDrive(char type, float PosSetpoint0, float PosSetpoint1,float VelSetpoint0, float VelSetpoint1,float CurSetpoint0, float CurSetpoint1);
void odriveSetup(int axis,float vel_limit,float current_limit);
void odriveSetupDual(float vel_limit,float current_limit);
float readAndDrive(int axis, char type, float setpoint);
int odriveReadFloat(float* data,int expected);
float readPosition(int axis);
float readVelocity(int axis);
float readCurrent(int axis);
float readVoltage();
float encoder();
#endif 